	
	<script type="text/javascript">
		$(document).ready(function(){
			show_subjects();
		});
		function show_subjects()
		{
			$.ajax({
				url: '<?php echo base_url('student/get_subjects') ?>',
				dataType: 'json',
				success: function(data)
				{
					var subjects = '';

					subjects = '<li class="active text-center"><a href="#" data-toggle="modal" data-target="#addSubjectModal"><span class="label label-primary"><i class="fa fa-plus"></i> Add Subject</span></li>';

					for (var i = 0; i < data.length; i++)
					{
						get_subject(data[i].access_code);
						subjects += '<li>' +
									'<a href="<?php echo base_url('student/subject') ?>/'+ data[i].access_code +'/<?php echo $user['firstname'] ?>/<?php echo $user['middlename'] ?>/<?php echo $user['lastname'] ?>"><span id="subject_'+ data[i].access_code +'"></span></a>' +
									'</li>';
					}

					$('#subjectsData').html(subjects);
				}
			});
		}

		$(document).on('submit', '#addSubjectForm', function(e){
			e.preventDefault();
			$('#submitting').show();
			$.ajax({
				url: '<?php echo base_url('student/add_subject') ?>',
				type: 'POST',
				data: $(this).serialize(),
				dataType: 'json',
				success: function(data)
				{
					if (data.subject_exists == 0)
					{
						$('#notif').html('<div class="alert alert-danger">Subject does not exists!</div>');
					}
					if (data.student_exists == 0)
					{
						$('#notif').html('<div class="alert alert-danger">You are not enrolled to this subject!</div>');
					}
					if (data.subject_exists == 0 && data.student_exists == 0)
					{
						$('#notif').html('<div class="alert alert-danger">Not found!</div>');
					}
					if (data.subject_exists == 1 && data.student_exists == 1)
					{
						$('#addSubjectModal').modal('hide');
					}
					$('#submitting').hide();
				}
			});
		});

		function get_subject(access_code)
		{
			$.ajax({
				url: '<?php echo base_url('student/get_subject_description') ?>',
				type: 'POST',
				data: {access_code: access_code},
				dataType: 'json',
				success: function(data)
				{
					$('#subject_' + access_code).text(data.subject_description);
				},
				error: function()
				{
					alert('Error!');
				}
			});
		}
	</script>

	<div id="addSubjectModal" class="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title"><i class="fa fa-plus"></i> Add Subject</h4>
				</div>
				<form id="addSubjectForm">
					<div class="modal-body">
						<div class="form-group text-center">
							<div id="notif"></div>
							<label>Enter your subject access code:</label>
							<input type="text" name="access_code" class="form-control text-center" required>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="firstname" value="<?php echo $user['firstname'] ?>">
						<input type="hidden" name="middlename" value="<?php echo $user['middlename'] ?>">
						<input type="hidden" name="lastname" value="<?php echo $user['lastname'] ?>">
						<button type="submit" class="btn btn-primary">Add</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y') ?> Online Class Portal.</strong> All rights
    reserved.
	</footer>
  
	</div>
</body>
</html>