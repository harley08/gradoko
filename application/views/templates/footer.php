
	<div id="addSubjectModal" class="modal modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title"><i class="glyphicon glyphicon-plus"></i> Add Subject</h4>
				</div>
				<form id="addSubjectForm">
					<?php
						if ($semester != null) {
							?>
					<div class="modal-body">
						<div class="form-group">
							<div class="row">
								<div class="col-md-5">
									<label>Subject:</label>
									<input type="text" name="subject_code" id="subject_code" class="form-control" placeholder="Subject Code" style="margin-bottom: 5px;" />
									<textarea name="subject_description" id="subject_description" class="form-control" placeholder="Subject Description" style="margin-bottom: 5px;"></textarea>
									<input type="number" name="no_of_sections" class="form-control" placeholder="No. of Section/s" />
								</div>
							</div>
						</div>
						<input type="hidden" name="semester_id" value="<?php echo $semester['id'] ?>" />
					</div>
					<div class="modal-footer">
						<span class="pull-left">
							<button type="submit" class="btn btn-primary">Add Subject</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</span>
					</div>
							<?php
						} else {
							?>
					<div class="modal-body">
						<h3 class="color-red text-center"><span class="glyphicon glyphicon-warning-sign"></span> Please activate semester and school year first on setting.</h3>
					</div>
					<div class="modal-footer">
						<span class="pull-left">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</span>
					</div>
							<?php
						}
					?>
				</form>
			</div>
		</div>
	</div>

	<div id="ViewSectionsModal" class="modal modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title" id="title"></h4>
				</div>
				<div class="modal-body">
					<div class="text-center"><h4>Choose section:</h4></div>
					<table class="table table-bordered table-hover">
						<tbody id="sectionsData"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">

		$(document).ready(function(){
			showSubjects();
		});
		function showSubjects() {
			$.ajax({
				url: '<?php echo base_url('subject/get'); ?>',
				type: 'POST',
				dataType: 'json',
				data: {semester_id: <?php echo $semester['id'] ?>},
				success: function(subjectdata) {
					var subjectLists = '';
					var i;

					subjectLists = '<li class="active text-center"><a href="#" data-toggle="modal" data-target="#addSubjectModal"><span class="label label-primary"><i class="fa fa-plus"></i> Add New</span></li>';
					for (i = 0; i < subjectdata.length; i++) {

						var semester_id = <?php echo $semester['id'] ?>;
						var subject_code = subjectdata[i].subject_code;

						subjectLists += '<li>' +
										'<a href="javascript:void(0);" class="view_sections" data-subject_id="' + subjectdata[i].id + '" data-semester_id="' + subjectdata[i].semester_id + '" data-subject_code="' + subjectdata[i].subject_code + '" data-subject_description="' + subjectdata[i].subject_description + '"><i class="fa fa-desktop"></i>' + subjectdata[i].subject_description +
										'</a>' +
										'</li>';
					}
					
					if (subjectdata.length === 0) {
						subjectLists += '<li class="text-center"><a href="#">no subjects yet</a></li>';
					}

					$('#subjectsData').html(subjectLists);
					$('#submitting').hide();
				}
			});
		}

		$('#subjectsData').on('click', '.view_sections', function(){

			var semester_id = $(this).data('semester_id');
			var subject_id = $(this).data('subject_id');
			var subject_code = $(this).data('subject_code');
			var subject_description = $(this).data('subject_description');

			$('#ViewSectionsModal').modal('show');
			$('#title').text(subject_description);

			$.ajax({

				url: '<?php echo base_url('subject/get_sections') ?>',
				type: 'POST',
				data: {semester_id: semester_id, subject_code: subject_code},
				dataType: 'json',
				success: function(sectionsData) {
					var sections;
					for (i = 0; i < sectionsData.length; i++) {
						sections += '<tr>' +
									'<td class="text-center"><i class="fa fa-desktop"></i> ' + sectionsData[i].section_name + '</td>' +
									'<td class="text-center"><a href="<?php echo base_url() ?>dashboard/subject/'+ subject_description +'/'+ subject_id + '/'+ subject_code +'/' + sectionsData[i].id + '" class="btn btn-primary btn-block"><i class="fa fa-folder-open"></i> Open</a></td>' +
									'</tr>';
					}

					$('#sectionsData').html(sections);
				}

			});

		});

		$('#addSubjectForm').submit(function(e){
			e.preventDefault();

			$('#submitting').show();

			$.ajax({
				type: 'POST',
				url: '<?php echo base_url('subject/add') ?>',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$('#submitting').show();
					$('#addSubjectModal').modal('hide');
					showSubjects();
					show_subjects();
				}
			});
		});
	</script>

	<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y') ?> Online Class Portal.</strong> All rights
    reserved.
	<div class="text-center">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Class Portal Ads -->
	<ins class="adsbygoogle"
		style="display:block"
		data-ad-client="ca-pub-4437781027424280"
		data-ad-slot="1512986940"
		data-ad-format="auto"
		data-full-width-responsive="true"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	</div>
	</footer>
  
	</div>
</body>
</html>