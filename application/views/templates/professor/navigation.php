		<header class="main-header">
		<!-- Logo -->
		<a href="<?php echo base_url(); ?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>C</b>P</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>CLASS</b> RECORD</span>
		</a>

		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>

			<!-- Navbar Right Menu -->
	      	<div class="navbar-custom-menu">
	      		<ul class="nav navbar-nav">
	      			<li class="dropdown user user-menu">
	      				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
              				<?php
              					if ($this->session->userdata('activated') == 1) {
									?>
							<img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              				<span class="hidden-xs"><?php echo $user['firstname'] . ' ' . $user['lastname']; ?></span>
									<?php
              					} else {
              					?>
              				<img src="<?php echo base_url() ?>assets/dist/img/no-avatar.jpg" class="user-image" alt="User Image">
              				<span class="hidden-xs">No Name Yet</span>
              					<?php	
              				}
              				?>
            			</a>
            			<ul class="dropdown-menu">
            				<li class="user-header">
            					<?php
            						if ($this->session->userdata('activated') == 1) {
            							?>
            					<img src="<?php echo base_url() ?>/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            					<p>
            						<?php echo $user['firstname'] . ' ' . $user['lastname'] . ' -  ' . $this->session->userdata['role'] ?>
            						<small><?php echo $user['teaching_at'] ?></small>
            					</p>
            							<?php
            						} else {
            							?>
            					<img src="<?php echo base_url() ?>/assets/dist/img/no-avatar.jpg" class="img-circle" alt="User Image">
            					<p>No Name Yet</p>
            							<?php	
            						}
            					?>
            				</li>
            				<li class="user-footer">
				                <!-- <div class="pull-left">
								<?php

									if ($this->session->userdata('activated') == 1) {
										?>

									<a href="<?php echo base_url('profile/professor/' . $user['profile_link']) ?>" class="btn btn-default btn-flat">Profile</a>

										<?php
									}

								?>
				                  	
				                </div> -->
				                <div class="pull-right">
				                  	<a href="<?php echo base_url('signout'); ?>" class="btn btn-default btn-flat">Sign out</a>
				                </div>
              				</li>
            			</ul>
	      			</li>
					<li>
						<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
					</li>
	      		</ul>
	      	</div>

		</nav>

	</header>