	
	<aside class="main-sidebar">
   <?php
    if ($this->session->userdata('activated') == 0) {
      ?>
      <div class="unactivated text-center">
        <img src="<?php echo base_url(); ?>assets/dist/img/no-avatar.jpg" class="img-circle" width="50%">
        <h4 class="color-red">Activate Account!</h4>
        <p><span class="glyphicon glyphicon-warning-sign"></span> Please update your profile to activate your account!</p>
      </div>
      <?php
    }
   ?>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle">
        </div>
        <div class="pull-left info">
          <P><?php echo $user['firstname'] . ' ' . $user['lastname']; ?></P>
          <small><?php echo $this->session->userdata('role'); ?></small>
        </div>
      </div>  
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">
          <?php
              if ($semester != null) {
                ?>
          <table width="100%">
            <tr>
              <td align="right" style="padding: 5px;"><b>School Year:</b></td>
              <td align="left" style="padding: 5px;"><?php echo $semester['school_year'] ?></td>
            </tr>
            <tr>
              <td align="right" style="padding: 5px;"><b>Semester:</b></td>
              <td align="left" style="padding: 5px;">
                <?php echo $semester['semester'] ?><br>
                <small>(<?php echo $semester['semestral_term'] ?>)</small>                  
                </td>
            </tr>
          </table>
                <?php
              } else {
                ?>
            <u class="color-red"><a href="<?php echo base_url('dashboard/semester') ?>" class="color-red">Set School Year and Active Semester</a></u>
                <?php
              }
           ?>
        </li>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-desktop"></i>
            <span>Subjects</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" id="subjectsData">
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cog"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('dashboard/semester'); ?>"><i class="fa fa-wrench"></i> Sem & S.Y. Activation</a></li>
            <li><a href="<?php echo base_url('dashboard/grading') ?>"><i class="fa fa-star"></i> Grading System</a></li>
            <li><a href="<?php echo base_url('dashboard/manage/subjects') ?>"><i class="fa fa-desktop"></i> Manage Subjects</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>