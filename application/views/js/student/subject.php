<script type="text/javascript">

	$(document).ready(function(){

		showStudentlList();
		showGrading();

	});

	function showStudentlList() {

		$.ajax({
			url: '<?php echo base_url('student/show_classmates') ?>',
			type: 'POST',
			dataType: 'json',
			data: {subject_id: '<?php echo $subjectData['id'] ?>', section_id: '<?php echo $section_id ?>'},
			success: function(data) {
				
				var studentList = '';
				var i;

				for (i = 0; i < data.length; i++) {
					studentList += '<tr>' +
									'<td class="text-center">' + data[i].lastname + '</td>' +
									'<td class="text-center">' + data[i].firstname + '</td>' +
									'<td class="text-center">' + data[i].middlename + '</td>' +
									'</tr>';
				}

				if (data.length === 0) {
					studentList = '<tr><td class="text-center" colspan="3">no data</td></tr>';
				}

				$('#studentsLists').html(studentList);
				
				$('#studentsTbl').DataTable({
					"paging": true,
					"lengthChange": false,
					"searching": true,
					"ordering": false,
					"info": true,
					"autoWidth": true
				});

			},
			error: function()
			{
				alert('Error!');
			}
		});

	}

	function showGrading() {
		$.ajax({
			url: '<?php echo base_url('subject/get_grading_student') ?>',
			type: 'POST',
			dataType: 'json',
			data: {subject_id: '<?php echo $subjectData['id'] ?>'},
			success: function(data) {
				var gradingList = '';

				for (var i = 0; i < data.length; i++) {

					id = data[i].id;
					no = i;

					countActivities(id, no);

					gradingList += '<div class="info-box">' +
									'<a href="javascript:void(0);" class="view_grading" data-base_grade="' + data[i].base_grade + '" data-grade_id="' + data[i].id + '"><span class="info-box-icon bg-green grading"><i class="fa fa-sticky-note-o"></i></span></a>' +
									'<div class="info-box-content">' +
									'<span class="info-box-text">' + data[i].base_grade + '</span>' +
									'<span class="info-box-number"><span id="activity_count_'+ i +'"></span><small></small></span>' +
									'</div>' +
									'</div>';
				}
				$('#showGrading').html(gradingList);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert('Error: ' + thrownError);
			}
		});
	}

	function countActivities(id, no)
	{		
		$.ajax({
			url: '<?php echo base_url('subject/show_activities_student') ?>',
			type: 'POST',
			data: {subject_id: <?php echo $subjectData['id'] ?>, section_id: <?php echo $section_id ?>, grade_id: id},
			dataType: 'json',
			success: function(data)
			{
				if (data.length != 0)
				{
					$('#activity_count_' + no).text(data.length);
				} else {
					$('#activity_count_' + no).text('0');
				}

			}
		});
	}

	$('#showGrading').on('click', '.view_grading', function(){
		var grade_id = $(this).data('grade_id');
		var base_grade = $(this).data('base_grade');

		$('#gradingModal').modal('show');
		$('#grading_title').text(base_grade);

		$.ajax({
			url: '<?php echo base_url('subject/show_activities_student') ?>',
			type: 'POST',
			data: {subject_id: <?php echo $subjectData['id'] ?>, section_id: <?php echo $section_id ?>, grade_id: grade_id},
			dataType: 'json',
			success: function(data)
			{
				var activities = '';
				var student_scores = '';

				for (var i = 0; i < data.length; i++)
				{
					activities += '<tr>' +
									'<td class="text-center">' + (i+1) + '</td>' +
									'<td>' + data[i].details + '</td>' +
									'<td>' + data[i].total_points + '</td>' +
									'<td>' + data[i].date_created + '</td>' +
									'</tr>';

					var first = '';
					if (i === 0)
					{
						first = 'in';
					}

					show_grades(i, data[i].id);

					student_scores += '<div class="panel box box-success">' +
										'<div class="box-header with-border">' +
										'<h4 class="box-title">' +
										'<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + i + '">' +
										data[i].details +
										'</a>' +
										'</h4>' +
										'<div id="collapse' + i + '" class="panel-collapse collapse '+ first +'">' +
										'<div class="box-body" id="collapseContent' + i + '"></div>' +
										'</div>' +
										'</div>' +
										'</div>';
				}

				if (data.length == 0)
				{
					activities = '<tr><td colspan="5" class="text-center">no data</td></tr>';
				}

				$('#activities').html(activities);
				$('#accordion').html(student_scores);

			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert('Error: ' + thrownError);
			}
		});
	});

	function show_grades(content_id, grades_id)
	{
		$.ajax({
			url: '<?php echo base_url('grade/show_per_activity_student_me') ?>',
			type: 'POST',
			data: {grading_id: grades_id, grade_id: content_id, subject_id: <?php echo $subjectData['id'] ?>, section_id: <?php echo $section_id ?>, student_id: <?php echo $student_id ?>},
			dataType: 'json',
			success: function(data)
			{
				var grades;

				grades = '<table class="table table-bordered table-hover">';
				grades += '<thead>';
					grades += '<tr>';
						grades += '<th class="text-center">Student</th>';
						grades += '<th class="text-center">Score</th>';
					grades += '<tr>';
				grades += '</thead>';
				grades += '<tbody>';

				for (var i = 0; i < data.length; i++)
				{
					grades += '<tr>' +
								'<td class="text-center">' + data[i].firstname + ' ' + data[i].lastname + '</td>' +
								'<td class="text-center">' +  data[i].points + '</td>' +
								'</tr>';
				}

				if (data.length == 0)
				{
					grades += '<tr><td colspan="2" class="text-center">no scores yet</td></tr>';
				}

				grades += '</tbody>';
				grades += '</table>';

				$('#collapseContent' + content_id).html(grades);
			}
		});
	}
</script>