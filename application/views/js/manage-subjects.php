<script type="text/javascript">

	$(document).ready(function(){
		show_subjects();
	});
	
	function show_subjects()
	{
		$.ajax({
			url: '<?php echo base_url('subject/get') ?>',
			type: 'POST',
			data: {semester_id: <?php echo $semester['id'] ?>},
			dataType: 'json',
			success: function(data)
			{
				var subjects = '';

				for (var i = 0; i < data.length; i++)
				{
					subjects += '<tr>' +
								'<td class="text-center">' + data[i].subject_code + '</td>' +
								'<td class="text-center">' + data[i].subject_description + '</td>' +
								'<td class="text-center"><b>' + data[i].no_of_sections + '<b></td>' +
								'<td class="text-center"><a href="javascript:void(0);" class="label bg-green edit_subject" data-id="' + data[i].id + '" data-subject_code="' + data[i].subject_code + '" data-subject_description="' + data[i].subject_description + '" data-no_of_sections="' + data[i].no_of_sections + '">Edit</a> <a href="javascript:void(0);" class="label bg-red delete_subject" data-id="' + data[i].id + '" data-subject_code="' + data[i].subject_code + '" data-subject_description="' + data[i].subject_description + '" data-no_of_sections="' + data[i].no_of_sections + '">Delete</a></td>' +
								'</tr>';
				}

				if (data.length == 0)
				{
					subjects += '<tr>' +
								'<td colspan="4" class="text-center">no data</td>' +
								'</tr>';
				}

				$('#subjects').html(subjects);
			}
		});
	}

	$('#subjects').on('click', '.edit_subject', function(){
		var id = $(this).data('id');
		var subject_code = $(this).data('subject_code');
		var subject_description = $(this).data('subject_description');
		var t_sections = $(this).data('no_of_sections');

		$('#u_code').val(subject_code);
		$('#u_curr_code').val(subject_code);
		$('#u_description').val(subject_description);
		$('#u_sections').val(t_sections);
		$('#u_id').val(id);

		$('#editSubjectModal').modal('show');
	});

	$('#subjects').on('click', '.delete_subject', function(){

		var id = $(this).data('id');
		var subject_code = $(this).data('subject_code');
		var subject_description = $(this).data('subject_description');
		var t_sections = $(this).data('no_of_sections');

		$('#subject_code_txt').text(subject_code);
		$('#subject_description_txt').text(subject_description);
		$('#u_del_subject').val(id);
		$('#u_del_subject_code').val(subject_code);

		$('#deleteSubjectModal').modal('show');

	});

	$('#editSubjectForm').submit(function(e){

		e.preventDefault();

		$.ajax({
			url: '<?php echo base_url('subject/update') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function(){
				$('#editSubjectModal').modal('hide');
				show_subjects();
				showSubjects();
			}
		});

	});

	$('#deleteSubjectForm').submit(function(e){
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url('subject/delete') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				show_subjects();
				showSubjects();
				$('#deleteSubjectModal').modal('hide');
			}
		});
	});

</script>