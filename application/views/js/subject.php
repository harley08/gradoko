<script type="text/javascript">

	$(document).ready(function(){

		checkAttendanceIfAddedToGrades();
		showStudentlList();
		showGrading();
		viewAttendance();
		showCheckStudentlList();
		show_subject_datesPerAttendance();
		showGradings();

		var studentAttendanceByDate = '<tr>' +
						 '<td class="text-center" colspan="3">select date first</td>' +
						 '</tr>';
		$('#studentAttendanceByDate').html(studentAttendanceByDate);

		$('#choose_date_attendance').change(function(){

			var date = $(this).val();

			$.ajax({
				url: '<?php echo base_url('student/show_attendance') ?>',
				type: 'POST',
				data: {subject_id: <?php echo $subjectData['id'] ?>, semester_id: <?php echo $semester['id'] ?>, section_id: <?php echo $section_id ?>, date: date},
				dataType: 'json',
				success: function(data)
				{
					var studentAttendanceByDate = '';

					for (var i = 0; i < data.length; i++) {
					
					var status = '';


					if (data[i].status == "present") {
						status = '<small class="label bg-green"><span class="fa fa-check"></span></small>';
					} else if (data[i].status == "late") {
						status = '<small class="label bg-orange"><span class="fa fa-check"></span></small>';
					} else {
						status = '<small class="label bg-red"><span class="fa fa-remove"></span></small>';
					}

					var middle_initial = data[i].middlename;

					studentAttendanceByDate += '<tr>' +
									  '<td>' + data[i].lastname + ', ' + data[i].firstname + ' ' + middle_initial.charAt(0) + '.</td>' +
									  '<td class="text-center">' + status + '</td>' +
									  '<td class="text-center"><span class="glyphicon glyphicon-edit"></span></td>' +
									  '</tr>';
				}

				if (data.length == 0)
				{
					studentAttendanceByDate = '<tr>' +
						 '<td class="text-center" colspan="3">select date first</td>' +
						 '</tr>';
				}

					$('#studentAttendanceByDate').html(studentAttendanceByDate);
				},
				error: function()
				{
					alert('Error!');
				}
			});

		});

	});

	function showCheckStudentlList() {

		$.ajax({
			url: '<?php echo base_url('student/show') ?>',
			type: 'POST',
			dataType: 'json',
			data: {subject_id: '<?php echo $subjectData['id'] ?>', section_id: '<?php echo $section_id ?>'},
			success: function(data) {
				
				var studentList = '';
				var i;

				for (i = 0; i < data.length; i++) {
					studentList +=  '<tr>' +
									'<td class="text-center" style="border: 1px solid #000;">' + data[i].lastname + '</td>' +
									'<td class="text-center" style="border: 1px solid #000; border-right: 1px solid #25AF00;">' + data[i].firstname + '</td>' +
									'<td class="text-center" style="border: 1px solid #25AF00;"><input type="radio" name="status[' + data[i].id +']" value="present" /></td>' +
									'<td class="text-center" style="border: 1px solid #DBCE00;"><input type="radio" name="status[' + data[i].id +']" value="late" /></td>' +
									'<td class="text-center" style="border: 1px solid #FF0000;"><input type="radio" name="status[' + data[i].id +']" value="absent" /></td>' +
									'<input type="hidden" name="student[]" value="' + data[i].id + '" />' +
									'</tr>';
				}

				if (data.length === 0) {
					studentList = '<tr><td class="text-center" colspan="4">no data</td></tr>';
				}

				$('#CheckStudentList').html(studentList);

			}
		});

	}

	function showStudentlList() {

		$.ajax({
			url: '<?php echo base_url('student/show') ?>',
			type: 'POST',
			dataType: 'json',
			data: {subject_id: '<?php echo $subjectData['id'] ?>', section_id: '<?php echo $section_id ?>'},
			success: function(data) {
				
				var studentList = '';
				var i;

				for (i = 0; i < data.length; i++) {
					studentList += '<tr>' +
									'<td class="text-center">' + data[i].lastname + '</td>' +
									'<td class="text-center">' + data[i].firstname + '</td>' +
									'<td class="text-center">' + data[i].middlename + '</td>' +
									'<td class="text-center"><a href="javascript:void(0);" class="edit_student label bg-green" data-id="' + data[i].id + '" data-firstname="' + data[i].firstname + '" data-middlename="' + data[i].middlename + '" data-lastname="' + data[i].lastname + '">Edit</a></td>' +
									'</tr>';
				}

				if (data.length === 0) {
					studentList = '<tr><td class="text-center" colspan="5">no data</td></tr>';
				}

				$('#totalStudents').html(data.length);

				$('#studentsLists').html(studentList);
				
				$('#studentsTbl').DataTable({
					"paging": true,
					"lengthChange": false,
					"searching": true,
					"ordering": false,
					"info": true,
					"autoWidth": true
				});

			}
		});

	}

	$('#studentsLists').on('click', '.edit_student', function(){
		var id = $(this).data('id');
		var firstname = $(this).data('firstname');
		var middlename = $(this).data('middlename');
		var lastname = $(this).data('lastname');

		$('#u_fname').val(firstname);
		$('#u_mname').val(middlename);
		$('#u_lname').val(lastname);
		$('#u_id').val(id);

		$('#UpdateStudentModal').modal('show');

	});

	$('#UpdateStudentForm').submit(function(e){

		e.preventDefault();

		$.ajax({

			url: '<?php echo base_url('student/update') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function() {
				$('#UpdateStudentModal').modal('hide');
				showStudentlList();
				showCheckStudentlList();
			}

		});

	});

	$('#addStudentForm').submit(function(e){

		e.preventDefault();

		$.ajax({
			url: '<?php echo base_url('student/add') ?>',
			type: 'POST',
			dataType: 'html',
			data: $(this).serialize(),
			success: function() {
				$('#AddStudentModal').modal('hide');
				showStudentlList();
				showCheckStudentlList();
				
			}
		});

	});

	$('#checkAttendanceForm').submit(function(e) {

		e.preventDefault();

		$('#submitting').show();

		$.ajax({

			url: '<?php echo base_url('student/attendance') ?>',
			type: 'POST',
			data: $(this).serialize(),
			dataType: 'html',
			success: function() {
				$('#checkAttendanceModal').modal('hide');
				$('#loading_message').text('Submitting your attendance...');
				$('#submitting').hide();
				$('#viewAttendanceModal').modal('show');
				viewAttendance();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert('Error: ' + thrownError);
				$('#submitting').hide();
			}

		});

	});

	function showGrading() {
		$.ajax({
			url: '<?php echo base_url('subject/get_grading') ?>',
			type: 'POST',
			dataType: 'json',
			data: {subject_id: '<?php echo $subjectData['id'] ?>'},
			success: function(data) {
				var gradingList = '';

				for (var i = 0; i < data.length; i++) {

					id = data[i].id;
					no = i;

					countActivities(id, no);

					gradingList += '<div class="info-box">' +
									'<a href="javascript:void(0);" class="view_grading" data-base_grade="' + data[i].base_grade + '" data-grade_id="' + data[i].id + '"><span class="info-box-icon bg-green grading"><i class="fa fa-sticky-note-o"></i></span></a>' +
									'<div class="info-box-content">' +
									'<span class="info-box-text">' + data[i].base_grade + ' <a href="javascript:void(0);" class="pull-right add_grade" data-grade_id="' + data[i].id + '" data-subject_id="' + data[i].subject_id + '" data-base_grade="' + data[i].base_grade + '"><b><i class="fa fa-plus"></i></b></a></span>' +
									'<span class="info-box-number"><span id="activity_count_'+ i +'"></span><small></small></span>' +
									'</div>' +
									'</div>';
				}
				$('#showGrading').html(gradingList);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert('Error: ' + thrownError);
			}
		});
	}

	function viewAttendance() {
		$.ajax({
			url: '<?php echo base_url('student/show_attendance') ?>',
			type: 'POST',
			data:  {subject_id: '<?php echo $subjectData['id'] ?>', semester_id: '<?php echo $semester['id'] ?>', section_id: '<?php echo $section_id ?>', date: '<?php echo date('Y-m-d') ?>'},
			dataType: 'json',
			success: function(data) {
				var AttendanceList = '';

				for (var i = 0; i < data.length; i++) {
					
					var status = '';


					if (data[i].status == "present") {
						status = '<small class="label bg-green"><span class="fa fa-check"></span></small>';
					} else if (data[i].status == "late") {
						status = '<small class="label bg-orange"><span class="fa fa-check"></span></small>';
					} else {
						status = '<small class="label bg-red"><span class="fa fa-remove"></span></small>';
					}

					var middle_initial = data[i].middlename;

					AttendanceList += '<tr>' +
									  '<td>' + data[i].lastname + ', ' + data[i].firstname + ' ' + middle_initial.charAt(0) + '.</td>' +
									  '<td class="text-center">' + status + '</td>' +
									  '<td class="text-center"><span class="glyphicon glyphicon-edit"></span></td>' +
									  '</tr>';

					$('#submitAttendance').attr('disabled', 'disabled');
					$('#submitAttendNotif').html('<div class="alert alert-danger">You already submitted an attendance.</div>');
				}

				if (data.length == "") {
					AttendanceList = '<tr>' +
									 '<td class="text-center" colspan="3">no attendance yet</td>' +
									 '</tr>';
				}

				$('#studentAttendance').html(AttendanceList);
			}
		});
	}

	$('#showGrading').on('click', '.add_grade', function(){
		var grade_id = $(this).data('grade_id');
		var subject_id = $(this).data('subject_id');
		var section_id = <?php echo $section_id ?>;
		var base_grade = $(this).data('base_grade');
		
		$('#addGradeModal').modal('show');
		$('#base_grade').text(base_grade);
		$('#get_grade_id').val(grade_id);
		$('#get_subject_id').val(subject_id);
		$('#get_section_id').val(section_id);

	});

	$('#addGradeForm').submit(function(e){
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url('grade/addTo_baseGrade') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				$('#addGradeModal').modal('hide');
				showGrading();
			}
		});
	});

	$('#showGrading').on('click', '.view_grading', function(){
		var grade_id = $(this).data('grade_id');
		var base_grade = $(this).data('base_grade');

		$('#gradingModal').modal('show');
		$('#grading_title').text(base_grade);

		$.ajax({
			url: '<?php echo base_url('subject/show_activities') ?>',
			type: 'POST',
			data: {subject_id: <?php echo $subjectData['id'] ?>, section_id: <?php echo $section_id ?>, grade_id: grade_id},
			dataType: 'json',
			success: function(data)
			{
				var activities = '';
				var student_scores = '';

				for (var i = 0; i < data.length; i++)
				{
					activities += '<tr>' +
									'<td class="text-center">' + (i+1) + '</td>' +
									'<td>' + data[i].details + '</td>' +
									'<td>' + data[i].total_points + '</td>' +
									'<td>' + data[i].date_created + '</td>' +
									'<td><a href="javascript:void(0);" class="edit_activity label bg-green" data-base_grade="'+ base_grade +'" data-grade_id="'+ data[i].id +'" data-details="'+ data[i].details +'" data-points="'+ data[i].total_points +'">Edit</a> <a a href="javascript:void(0);" data-base_grade="'+ base_grade +'" data-grade_id="'+ data[i].id +'" data-details="'+ data[i].details +'" data-points="'+ data[i].total_points +'" class="delete_activity label bg-red">Delete</a></td>' +
									'</tr>';

					var first = '';
					if (i === 0)
					{
						first = 'in';
					}

					show_grades(i, data[i].id);

					student_scores += '<div class="panel box box-success">' +
										'<div class="box-header with-border">' +
										'<h4 class="box-title">' +
										'<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + i + '">' +
										data[i].details +
										'</a>' +
										'</h4>' +
											'<div class="box-tools pull-right">' +
												'<div class="btn-group">' +
													'<button type="button" class="btn btn-box-tool dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">' +
														'<i class="fa fa-caret-square-o-down"></i>' +
													'</button>' +
													'<ul class="dropdown-menu" role="menu">' +
														'<li><a onclick="encodeGrade(' + i + ', ' + data[i].id + ')"><i class="fa fa-pencil"></i> Encode Grades</a><li>' +
														'<li><a onclick="show_grades(' + i + ', ' + data[i].id + ')"><i class="fa fa-search-plus"></i> Show Grades</a><li>' +
													'</ul>' +
												'</div>' +
											'</div>' +
										'</div>' +
										'<div id="collapse' + i + '" class="panel-collapse collapse '+ first +'">' +
										'<div class="box-body" id="collapseContent' + i + '"></div>' +
										'</div>' +
										'</div>';
				}

				if (data.length == 0)
				{
					activities = '<tr><td colspan="5" class="text-center">no data</td></tr>';
				}

				$('#activities').html(activities);
				$('#accordion').html(student_scores);

			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert('Error: ' + thrownError);
			}
		});
	});

	function encodeGrade(content_id, grades_id) 
	{
		$.ajax({
			url: '<?php echo base_url('student/show') ?>',
			type: 'POST',
			data: {semester_id: '<?php echo $semester['id'] ?>', subject_id: '<?php echo $subjectData['id'] ?>', section_id: '<?php echo $section_id ?>', grade_id: grades_id},
			dataType: 'json',
			success: function(data)
			{
				var encoding;
				encoding = '<div class="alert alert-info">Encoding Grades...</div>';
				encoding = '<form id="EncodeGradesForm">';
				encoding += '<input type="hidden" name="subject_id" value="<?php echo $subjectData['id'] ?>">';
				encoding += '<input type="hidden" name="section_id" value="<?php echo $section_id ?>">';
				encoding += '<input type="hidden" name="grading_id" value="' + grades_id + '">';
				encoding += '<input type="hidden" name="grade_id" value="' + content_id + '">';
				encoding += '<table class="table table-bordered table-border">';
				encoding += '<thead>';
					encoding += '<tr>';
						encoding += '<td class="text-center">Student</td>';
						encoding += '<td class="text-center">Grade</td>';
					encoding += '</tr>';
				encoding += '</thead>';
				encoding += '<tbody>';

				for (var i = 0; i < data.length; i++)
				{
					encoding += '<tr>' +
									'<td width="90%" class="text-center">' + data[i].firstname + ' ' + data[i].lastname + '</td>' +
									'<td width="10%" class="text-center"><input type="hidden" name="student[]" value="' + data[i].id + '" /><input type="number" name="score[' + data[i].id + ']" class="form-control" style="width: 100px; margib: auto;" /></td>' +
								'</tr>';
				}

				encoding += '</tbody>';
				encoding += '</table>';
				encoding += '<button type="submit" class="btn btn-primary btn-block">Submit Grade/s</button>';
				encoding += '</form>';

				$('#collapseContent' + content_id).html(encoding);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert('Error: ' + thrownError);
			}
		});
	}

	function countActivities(id, no)
	{		
		$.ajax({
			url: '<?php echo base_url('subject/show_activities') ?>',
			type: 'POST',
			data: {subject_id: <?php echo $subjectData['id'] ?>, section_id: <?php echo $section_id ?>, grade_id: id},
			dataType: 'json',
			success: function(data)
			{
				if (data.length != 0)
				{
					$('#activity_count_' + no).text(data.length);
				} else {
					$('#activity_count_' + no).text('0');
				}

			}
		});
	}

	$('#activities').on('click', '.edit_activity', function(){

		var base_grade = $(this).data('base_grade');
		var id = $(this).data('grade_id');
		var details = $(this).data('details');
		var total_points = $(this).data('points');

		$('#u_base_grade').text(base_grade);
		$('#u_total_points').val(total_points);
		$('#u_details').val(details);
		$('#u_grade_id').val(id);

		$('#gradingModal').modal('hide');
		$('#updateGradeModal').modal('show');
	});

	$('#updateGradeForm').submit(function(e){
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url('activity/update') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				$('#updateGradeModal').modal('hide');
				showGrading();
			}
		});
	});

	$('#activities').on('click', '.delete_activity', function(){

		var base_grade = $(this).data('base_grade');
		var id = $(this).data('grade_id');
		var details = $(this).data('details');
		var total_points = $(this).data('points');

		$('#gradingModal').modal('hide');
		$('#del_base_grade').text(base_grade);
		$('#delete_details').text(details);
		$('#delete_total_points').text(total_points);
		$('#del_id').val(id);

		$('#deleteActivityModal').modal('show');
		
	});

	$('#deleteActivityForm').submit(function(e){
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url('activity/delete') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				$('#deleteActivityModal').modal('hide');
				showGrading();
				
			}
		});
	});

	$(document).on('submit', '#EncodeGradesForm', function(e){

		e.preventDefault();

		$.ajax({
			url: '<?php echo base_url('grade/encode') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				$('#gradingModal').modal('hide');
			}
		});
	});

	function show_grades(content_id, grades_id)
	{
		$.ajax({
			url: '<?php echo base_url('grade/show_per_activity') ?>',
			type: 'POST',
			data: {grading_id: grades_id, grade_id: content_id, subject_id: <?php echo $subjectData['id'] ?>, section_id: <?php echo $section_id ?>},
			dataType: 'json',
			success: function(data)
			{
				var grades;

				grades = '<table class="table table-bordered table-hover">';
				grades += '<thead>';
					grades += '<tr>';
						grades += '<th class="text-center">Student</th>';
						grades += '<th class="text-center">Score</th>';
						grades += '<th class="text-center">Action</th>';
					grades += '<tr>';
				grades += '</thead>';
				grades += '<tbody>';

				for (var i = 0; i < data.length; i++)
				{
					grades += '<tr>' +
								'<td class="text-center">' + data[i].firstname + ' ' + data[i].lastname + '</td>' +
								'<td class="text-center">' +  data[i].points + '</td>' +
								'<td class="text-center"><a onclick="updateScore('+ data[i].id +')" class="label bg-green">Edit</a></td>' +
								'</tr>';
				}

				if (data.length == 0)
				{
					grades += '<tr><td colspan="3" class="text-center">no scores yet</td></tr>';
				}

				grades += '</tbody>';
				grades += '</table>';

				$('#collapseContent' + content_id).html(grades);
			}
		});
	}

	function updateScore(id)
	{

		$.ajax({
			url: '<?php echo base_url('grade/show_update_grade') ?>',
			type: 'POST',
			data: {id: id},
			dataType: 'json',
			success: function(data)
			{
				$('#student_name').text(data[0].firstname + ' ' + data[0].lastname);
				$('#u_score').val(data[0].points);
				$('#u_score_id').val(id);

				$.ajax({
					url: '<?php echo base_url('grade/get_activity') ?>',
					type: 'POST',
					data: {id: data[0].grading_id},
					dataType: 'json',
					success: function(data2)
					{
						$('#activity_name').text(data2[0].details);
					}
				});

			}
		});

		$('#gradingModal').modal('hide');
		$('#updateScoreModal').modal('show');
	}

	$('#updateScoreForm').submit(function(e){

		e.preventDefault();

		$.ajax({
			url: '<?php echo base_url('grade/update_score') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				$('#updateScoreModal').modal('hide');
			}

		});

	});

	function show_subject_datesPerAttendance()
	{
		$.ajax({
			url: '<?php echo base_url('subject/get_all_attendanceDate') ?>',
			type: 'POST',
			data: {subject_id: <?php echo $subjectData['id'] ?>, semester_id: <?php echo $semester['id'] ?>, section_id: <?php echo $section_id ?>},
			dataType: 'json',
			success: function(data)
			{
				var show_attendanceDates = "<option value=''>-- select date --</option>";

				for (var i = 0; i < data.length; i++)
				{
					show_attendanceDates += '<option value="'+ data[i].date +'">'+ data[i].date +'</option>';
				}

				var total_attendance = data.length;
				$('#total_points_txt').val(total_attendance);

				$('#choose_date_attendance').html(show_attendanceDates);
			},
			error: function()
			{
				alert('Error!');
			}
		});
	}

	$(document).on('click', '#addToGradingBtn', function(){
		$('#viewAttendanceModal').modal('hide');
		$('#addToGradingModal').modal('show');
	});

	function showGradings()
	{
		$.ajax({
			url: '<?php echo base_url('subject/get_grading') ?>',
			type: 'POST',
			data: {subject_id: <?php echo $subjectData['id'] ?>},
			dataType: 'json',
			success: function(data)
			{
				var gradingsFor = '';

				for (var i = 0; i < data.length; i++)
				{
					gradingsFor += '<div class="radio">' +
										'<label>' +
											'<input type="radio" name="grade_id" id="grade_id_'+ i +'" value="'+ data[i].id +'"> ' +
											data[i].base_grade +
										'</label>' +
									'</div>';
				}

				$('#gradingsFor').html(gradingsFor);
			}
		});
	}	

	$('#addToGradingForm').submit(function(e){
		e.preventDefault();

		if ($('#grade_id_0').is(':checked'))
		{
			$('#grading_id_txt').val('0');
		}
		if ($('#grade_id_1').is(':checked'))
		{
			$('#grading_id_txt').val('1');
		}
		if ($('#grade_id_2').is(':checked'))
		{
			$('#grading_id_txt').val('2');
		}
		if ($('#grade_id_3').is(':checked'))
		{
			$('#grading_id_txt').val('3');
		}
		if ($('#grade_id_4').is(':checked'))
		{
			$('#grading_id_txt').val('4');
		}
		if ($('#grade_id_5').is(':checked'))
		{
			$('#grading_id_txt').val('5');
		}
		if ($('#grade_id_6').is(':checked'))
		{
			$('#grading_id_txt').val('6');
		}
		if ($('#grade_id_7').is(':checked'))
		{
			$('#grading_id_txt').val('7');
		}
		if ($('#grade_id_8').is(':checked'))
		{
			$('#grading_id_txt').val('8');
		}
		if ($('#grade_id_9').is(':checked'))
		{
			$('#grading_id_txt').val('9');
		}
		if ($('#grade_id_10').is(':checked'))
		{
			$('#grading_id_txt').val('10');
		}

		$('#submitting').show();

		$.ajax({
			url: '<?php echo base_url('grade/addAttendance_ToBaseGrade') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function()
			{
				$('#addToGradingModal').modal('hide');
				$('#submitting').hide();
				checkAttendanceIfAddedToGrades();
				showGrading();
			}
		});
	});

	function checkAttendanceIfAddedToGrades()
	{
		var subject_id = <?php echo $subjectData['id'] ?>;
		var section_id = <?php echo $section_id ?>;

		$.ajax({
			url: '<?php echo base_url('grade/checkAttendanceIfAddedToGrade') ?>',
			type: 'POST',
			data: {subject_id: subject_id, section_id: section_id},
			dataType: 'json',
			success: function(data)
			{
				if (data.exist != 0)
				{
					var notif = '';
					notif = '<div class="alert alert-danger">Attendance already added!</div>';
					$('#addAttendanceToGradeBtn').prop('disabled', true);
					$('#addAttendanceToGradeNotif').html(notif);
				}
			}
		});
	}

</script>