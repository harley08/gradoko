<div id="AddStudentModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-plus"></i> Add Student</h4>
			</div>
			<form id="addStudentForm">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Name:</label>
								<input type="text" name="fname" class="form-control" placeholder="First Name" style="margin-bottom: 5px" />
								<input type="text" name="mname" class="form-control" placeholder="Middle Name" style="margin-bottom: 5px" />
								<input type="text" name="lname" class="form-control" placeholder="Last Name" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" name="semester_id" value="<?php echo $semester['id'] ?>" />
						<input type="hidden" name="subject_id" value="<?php echo $subjectData['id'] ?>" />
						<input type="hidden" name="section_id" value="<?php echo $section_id ?>" />
						<button type="submit" class="btn btn-primary">Add Student</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="UpdateStudentModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-edit"></i> Update Student</h4>
			</div>
			<form id="UpdateStudentForm">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Name:</label>
								<input type="text" name="fname" id="u_fname" class="form-control" placeholder="First Name" style="margin-bottom: 5px" />
								<input type="text" name="mname" id="u_mname" class="form-control" placeholder="Middle Name" style="margin-bottom: 5px" />
								<input type="text" name="lname" id="u_lname" class="form-control" placeholder="Last Name" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" name="semester_id" value="<?php echo $semester['id'] ?>" />
						<input type="hidden" name="subject_id" value="<?php echo $subjectData['id'] ?>" />
						<input type="hidden" name="section_id" value="<?php echo $section_id ?>" />
						<input type="hidden" name="id" id="u_id" />
						<button type="submit" class="btn btn-primary">Update Student</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="checkAttendanceModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><?php echo date("F j, Y"); ?></h4>
			</div>
			<form id="checkAttendanceForm">
				<div class="modal-body">
				<div id="submitAttendNotif"></div>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td class="text-center" style="border: 1px solid #25AF00;">Present</td>
								<td class="text-center" style="border: 1px solid #DBCE00;">Late</td>
								<td class="text-center" style="border: 1px solid #FF0000;">Absent</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-hover" style="border: 1px solid #000;">
						<thead>
							<tr>
								<th class="text-center" style="border: 1px solid #000;">Last Name</th>
								<th class="text-center" style="border: 1px solid #000;">First Name</th>
								<th class="text-center" style="border: 1px solid #000;" colspan="3">Action</th>
							</tr>
						</thead>
						<tbody id="CheckStudentList"></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" name="semester_id" value="<?php echo $semester['id'] ?>" />
						<input type="hidden" name="subject_id" value="<?php echo $subjectData['id'] ?>" />
						<input type="hidden" name="section_id" value="<?php echo $section_id ?>" />
						<button id="submitAttendance" type="submit" class="btn btn-primary">Submit Attendance</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="viewAttendanceModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-calendar-check-o"></i> Attendance</h4>
			</div>
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#attendanceToday" data-toggle="tab">Attendance Today</a>
					</li>
					<li>
						<a href="#allAttendance" data-toggle="tab">All Attendance</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="attendanceToday">
						<p><b><?php echo date("F j, Y"); ?></b></p>
						<span class="pull-right">
							<small class="label bg-green"><span class="fa fa-check"></span></small> = Present, 
							<small class="label bg-orange"><span class="fa fa-check"></span></small> = Late, 
							<small class="label bg-red"><span class="fa fa-remove"></span></small> = Absent
							<div style="margin: 10px;"></div>
						</span>			
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">Student</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody id="studentAttendance"></tbody>
						</table>
					</div>
					<div class="tab-pane" id="allAttendance">
					<div class="pull-right">
						<button type="button" id="addToGradingBtn" class="btn btn-primary">Add attendance to Grading</button>
					</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Choose Date:</label>
									<select name="date" id="choose_date_attendance" class="form-control">
										<option value="">-- choose date --</option>
									</select>
								</div>
							</div>
						</div>
						<span class="pull-right">
							<small class="label bg-green"><span class="fa fa-check"></span></small> = Present, 
							<small class="label bg-orange"><span class="fa fa-check"></span></small> = Late, 
							<small class="label bg-red"><span class="fa fa-remove"></span></small> = Absent
							<div style="margin: 10px;"></div>
						</span>	
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">Student</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody id="studentAttendanceByDate"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="addGradeModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-plus"></i> Add (<b><span id="base_grade"></span></b>)</h4>
			</div>
			<form id="addGradeForm">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-8">
								<label>Details (Required):</label>
								<textarea class="form-control" name="details"></textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Total Points:</label>
								<input type="text" name="total_points" class="form-control" placeholder="e.g. 100" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="subject_id" id="get_subject_id" />
					<input type="hidden" name="section_id" id="get_section_id" />
					<input type="hidden" name="grade_id" id="get_grade_id" />					
					<button type="submit" class="btn btn-primary">Add</button>
					<button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="updateGradeModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-plus"></i> Update (<b><span id="u_base_grade"></span></b>)</h4>
			</div>
			<form id="updateGradeForm">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-8">
								<label>Details (Required):</label>
								<textarea class="form-control" name="details" id="u_details"></textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Total Points:</label>
								<input type="text" name="total_points" id="u_total_points" class="form-control" placeholder="e.g. 100" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="u_grade_id" />			
					<button type="submit" class="btn btn-primary">Update</button>
					<button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="deleteActivityModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i class="fa fa-remove"></i> Remove (<b><span id="del_base_grade"></span></b>)</h4>
			</div>
			<form id="deleteActivityForm">
				<div class="modal-body">
				<h3>Are you sure you want to delete this?</h3>
				<p>
					<b>Details:</b> <span id="delete_details"></span>
				</p>
				<p>
					<b>Total Points:</b> <span id="delete_total_points"></span>
				</p>
				</div>
				<div class="modal-footer">
				<input type="hidden" name="id" id="del_id" />
					<button type="submit" class="btn btn-primary">Yes</button>
					<button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="gradingModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button data-dismiss="modal" class="close">x</button>
				<h4 class="modal-title"><span id="grading_title"></span></h4>
			</div>
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
					<li><a href="#students_tab" data-toggle="tab">Scores</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="activity">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Details</th>
									<th class="text-center">Total Points</th>
									<th class="text-center">Date</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody id="activities"></tbody>
						</table>
					</div>
					<div class="tab-pane" id="students_tab">
						<div class="box-group" id="accordion">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="updateScoreModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-edit"></i> Update Score</h4>
			</div>
			<form id="updateScoreForm">
				<div class="modal-body">
					<div class="alert alert-danger">Updating score for <b>"<span id="activity_name"></span>"</b>.</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Name:</label>
								<p><span id="student_name"></span></p>
							</div>
							<div class="form-group">
								<label>Score:</label>
								<input type="text" name="points" id="u_score" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="u_score_id">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="addToGradingModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-plus"></i> Add to Grading</h4>
			</div>
			<form id="addToGradingForm">
				<div class="modal-body">
					<div class="form-group">
						<label>Choose where to add attendance:</label>
						<div id="gradingsFor"></div>
						<div id="addAttendanceToGradeNotif"></div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="subject_id" value="<?php echo $subjectData['id'] ?>" />
					<input type="hidden" name="section_id" value="<?php echo $section_id ?>" />
					<input type="hidden" name="details" value="Student Attendance" />
					<input type="hidden" name="total_points" id="total_points_txt" />
					<input type="hidden" name="grading_id" id="grading_id_txt" />
					<button type="submit" class="btn btn-primary" id="addAttendanceToGradeBtn">Add</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>