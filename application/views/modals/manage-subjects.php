<div id="editSubjectModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-edit"></i> Edit Subject</h4>
			</div>
			<form id="editSubjectForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Subject Code:</label>
								<input type="text" name="code" id="u_code" class="form-control">
							</div>
							<div class="form-group">
								<label>Subject Description:</label>
								<textarea name="description" id="u_description" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label>No of Sections:</label>
								<input type="text" name="sections" id="u_sections" class="form-control" disabled>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="u_id">
					<input type="hidden" name="curr_code" id="u_curr_code">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="deleteSubjectModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title"><i class="fa fa-remove"></i> Delete</h4>
			</div>
			<form id="deleteSubjectForm">
				<div class="modal-body">
					<h3 class="text-center">
						Are you sure you want to delete/remove this subject? <b>"<span id="subject_code_txt"></span> <span id="subject_description_txt"></span>"</b>
					</h3>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="u_del_subject">
					<input type="hidden" name="subject_code" id="u_del_subject_code">
					<button type="submit" class="btn btn-primary">Yes</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				</div>
			</form>
		</div>
	</div>
</div>