<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="<?php echo base_url() ?>assets/dist/img/favicon/icon.png">
	<title><?php echo $site ?></title>
	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<meta name="description" content="Online Class Record Portal">
    <meta name="author" content="Developed by: Harley Ferrer Lumagui">
  	<!-- Bootstrap 3.3.6 -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">
  	<!-- Pace style -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/pace/pace.min.css') ?>">
  	<script src="<?php echo base_url('assets/plugins/pace/pace.min.js') ?>"></script>
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/styles.css">

</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo base_url() ?>"><b>UPDATE</b> PROFILE</a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg color-red">You must update your profile to continue.</p>

			<?php echo $this->session->flashdata('message'); ?>
			<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

			<form method="post" action="" style="margin-bottom: 20px;">
				<div class="form-group">
					<input type="text" name="fname" class="form-control" placeholder="First Name" />
				</div>
				<div class="form-group">
					<input type="text" name="mname" class="form-control" placeholder="Middle Name" />
				</div>
				<div class="form-group">
					<input type="text" name="lname" class="form-control" placeholder="Last Name" />
				</div>
				<div class="form-group">
					<input type="text" name="mobile" class="form-control" placeholder="CP #" />
				</div>
				<div class="form-group">
					<input type="text" name="address" class="form-control" placeholder="Address" />
				</div>
				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
					</div>
				</div>
			</form>

			<p class="text-center">
				<a href="<?php echo base_url('signout') ?>">Logout</a>
			</p>

		</div>
	</div>
</body>
</html>