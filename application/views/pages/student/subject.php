<div class="content-wrapper">

	<?php
		$prof_id = $subjectData['prof_id'];

		$getprofessor = $this->db->query("SELECT * FROM professors WHERE userid = $prof_id");
		$prof = $getprofessor->row();

		$professor = $prof->firstname . ' ' . $prof->middlename . ' ' . $prof->lastname;

	?>

	<section class="content-header">
		<h1><b><?php echo $subjectData['subject_code']; ?> <?php echo $subjectData['subject_description'] ?> - <?php echo $sectionData['section_name']  ?></b></h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
			<li>Subject</li>
			<li><?php echo $subjectData['subject_description']; ?></li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-body">
						<p>
							<b>Professor:</b> <?php echo $professor ?>
						</p>
					</div>
				</div>
				<div class="box box-default">
					<div class="box-header with-border">
						<b>Co-Students List</b>
					</div>
					<div class="box-body">
						<!-- <form method="post" action="<?php echo base_url('grade/show_update_grade') ?>">
							<input type="text" name="id">
							<button type="submit">Submit</button>
						</form> -->
						<table id="studentsTbl" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">Last Name</th>
									<th class="text-center">First Name</th>
									<th class="text-center">Middle Name</th>
								</tr>
							</thead>
							<tbody id="studentsLists"></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="showGrading">
			</div>
		</div>
	</section>
</div>

<div id="gradingModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button data-dismiss="modal" class="close">x</button>
				<h4 class="modal-title"><span id="grading_title"></span></h4>
			</div>
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
					<li><a href="#students_tab" data-toggle="tab">Scores</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="activity">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Details</th>
									<th class="text-center">Total Points</th>
									<th class="text-center">Date</th>
								</tr>
							</thead>
							<tbody id="activities"></tbody>
						</table>
					</div>
					<div class="tab-pane" id="students_tab">
					<div class="alert alert-info">
						<b>Note:</b> You could only view your own records.
					</div>
						<div class="box-group" id="accordion">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>