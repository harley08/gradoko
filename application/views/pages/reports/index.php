<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="<?php echo base_url() ?>assets/dist/img/favicon/icon.png">
	<title><?php echo $site ?></title>
	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<meta name="description" content="Online Class Record Portal">
   
  	<!-- Bootstrap 3.3.6 -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">
  	<!-- Pace style -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/pace/pace.min.css') ?>">
  	<script src="<?php echo base_url('assets/plugins/pace/pace.min.js') ?>"></script>

	<style type="text/css">
		body {
			font-size: 10px;
		}
		@media print
		{
			#non-printable { display: none; }
			#printable { display: block; }
		}

	</style>

  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body style="width: 95%; margin: auto;">
<div id="non-printable">
	<div style="margin: 50px;"></div>
    <a href="#" onclick="print()" style="font-size: 13px !important;"><i class="fa fa-print"></i> Print</a>
</div>
<div id="printable">
	<h3 class="text-center">CLASS RECORDS</h3>
	<h3 class="text-center"><?php echo $semester['semester'] . ', School Year ' . $semester['school_year'] ?></h3>
	<h3 class="text-center"><?php echo strtoupper($semester['semestral_term']) ?> GRADE</h3>
	<h4 class="text-center"><?php echo strtoupper($subjectData['subject_code'] . ' - ' . $subjectData['subject_description']) ?></h4>

	<?php
		$semester_id = $semester['id'];
		$prof_id = $this->session->userdata('userid');
		$subject_id = $subjectData['id'];
		$grading = $this->db->query("SELECT * FROM grading WHERE prof_id = $prof_id AND subject_id = $subject_id");

		$get_grading = $this->db->query("SELECT * FROM grading WHERE prof_id = $prof_id AND subject_id = $subject_id");

		$count_grading = 0;

		foreach($get_grading->result_array() as $row)
		{
			$grade_id = $row['id'];
			$get_grade = $this->db->query("SELECT * FROM grade WHERE prof_id = $prof_id AND subject_id = $subject_id AND section_id = $section_id AND grade_id = $grade_id");

			foreach($get_grade->result_array() as $gradeData)
			{
				$count_grading += 2;
			}

		}

		$count_grading = $count_grading + $get_grading->num_rows();

	?>

    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th class="text-center" rowspan="3" colspan="1" valign="middle" style="padding-bottom: 43px">Students</th>
                <th class="text-center" rowspan="1" colspan="<?php echo $count_grading; ?>">Grading</th>
                <th class="text-center" rowspan="3" colspan="1	" style="padding-bottom: 43px">Total <b style="color: #ff0000">(100%)</b></th>
                <th class="text-center" rowspan="3" style="padding-bottom: 43px">Numerical Rating</th>
                <th class="text-center" rowspan="3" style="padding-bottom: 43px">Remarks</th>
            </tr>
			
			<?php

				// get grading
				$get_grading = $this->db->query("SELECT * FROM grading WHERE prof_id = $prof_id AND subject_id = $subject_id");

				echo '<tr>';

				foreach($get_grading->result_array() as $row)
				{
					$grade_id = $row['id'];
					$get_rows = $this->db->query("SELECT * FROM grade WHERE grade_id = $grade_id");

					?>
					<th colspan="<?php echo ($get_rows->num_rows() * 2); ?>" rowspan="1" class="text-center"><?php echo $row['base_grade'] ?></th>
					<th class="text-center" rowspan="2" style="padding-bottom: 20px"><b style="color: #ff0000">(<?php echo ($row['percentage'] * 100) ?>%)</b></th>
					<?php

				}

				echo '</tr>';

			?>

			<tr>
			
				<?php

				foreach($get_grading->result_array() as $row)
				{
					$grade_id = $row['id'];
					$get_grade = $this->db->query("SELECT * FROM grade WHERE prof_id = $prof_id AND subject_id = $subject_id AND section_id = $section_id AND grade_id = $grade_id");

					foreach($get_grade->result_array() as $gradeData)
					{
						?>
						<th class="text-center"><?php echo $gradeData['total_points'] ?></th>
						<th class="text-center"><font color="red">100%</font></th>
						<?php
					}

					if ($get_grade->num_rows() == null)
					{
						?>
						<th class="text-center" rowspan="1"><span style="color: #ff0000">no grades yet</span></th>
						<?php
					}

				}

				?>

			</tr>

        </thead>
		<tbody>
			<?php
				$students = $this->db->query("SELECT * FROM students WHERE prof_id = $prof_id AND semester_id = $semester_id AND subject_id = $subject_id AND section_id = $section_id");
				foreach ($students->result_array() as $row)
				{
					$student_id = $row['id'];
					?>
					<tr>
					<td class="text-center"><b><?php echo $row['lastname'] . ', ' . $row['firstname'] . ' ' . $row['middlename'] ?></b></td>
					
					<?php

					$total_percentage = 0;
				
					foreach($get_grading->result_array() as $row)
					{
						$grading_id = $row['id'];
						$get_grade = $this->db->query("SELECT * FROM grade WHERE prof_id = $prof_id AND subject_id = $subject_id AND section_id = $section_id AND grade_id = $grading_id");

						$total_per_grading = 0;

						$possible_total_score = 1;

						if ($get_grade->num_rows() == null)
						{
							?>
							<td class="text-center"><span style="color: #ff0000">no grades yet</span></td>
							<?php
						}

						foreach($get_grade->result_array() as $gradeData)
						{
							$possible_total_score += $gradeData['total_points'];
							$grade_id = $gradeData['id'];

							$get_grades = $this->db->query("SELECT * FROM grades WHERE grading_id = $grade_id AND student_id = $student_id");
							
							foreach($get_grades->result_array() as $student_grade)
							{
								$total_per_grading += $student_grade['points'];
								?>
								<td class="text-center"><?php echo $student_grade['points'] ?></td>
								<td class="text-center"><?php echo round(($student_grade['points'] / $gradeData['total_points'] ) * 50 + 50,2) ?></td>
								<?php
							}

							if ($get_grades->num_rows() == null)
							{
								?>
								<td class="text-center"><span style="color: #ff0000">0</span></td>
								<?php
							}
						}

						$no_of_grades = $get_grade->num_rows();

						$total_grade = ($total_per_grading / $possible_total_score * 50) + 50;

						$total_percentage += ($total_grade * $row['percentage']);

						?>
						<td class="text-center"><b><?php echo round(($total_grade * $row['percentage']), 2); ?>%</b></td>
						<?php

					}

					?>

					<?php

					$grade = "0";
					$remarks = "<span style='color: #FF0000'>Failed!</span>";

					$total_percentage = round($total_percentage, 0);

					switch($total_percentage)
					{
						case $total_percentage >= 96 and $total_percentage <= 100:
						$grade = "1.00";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 94 and $total_percentage <= 95:
						$grade = "1.25";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 91 and $total_percentage <= 93:
						$grade = "1.50";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 88 and $total_percentage <= 90:
						$grade = "1.75";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 85 and $total_percentage <= 87:
						$grade = "2.00";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 83 and $total_percentage <= 84:
						$grade = "2.25";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 80 and $total_percentage <= 82:
						$grade = "2.50";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 78 and $total_percentage <= 79:
						$grade = "2.75";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage >= 75 and $total_percentage <= 77:
						$grade = "3.00";
						$remarks = "<span style='color: #008e0d'><b>Pass!</b></span>";
						break;
						case $total_percentage < 75:
						$grade = "5.00";
						$remarks = "<span style='color: #FF0000'><b>Failed!</b></span>";
						break;
					}

					?>

					<td class="text-center"><b><?php echo round($total_percentage, 2) ?>%</b></td>
					<td class="text-center"><b><?php echo $grade ?></b></td>
					<td class="text-center"><?php echo $remarks ?>
					</tr>
					<?php
				}
			?>
		</tbody>
    </table>
</div>
</body>
</html>