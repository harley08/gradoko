<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="<?php echo base_url() ?>assets/dist/img/favicon/icon.png">
	<title><?php echo $site ?></title>
	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<meta name="description" content="Online Class Record Portal">
    
  	<!-- Bootstrap 3.3.6 -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">
  	<!-- Pace style -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/pace/pace.min.css') ?>">
  	<script src="<?php echo base_url('assets/plugins/pace/pace.min.js') ?>"></script>

  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo base_url() ?>"><b>CLASS</b> RECORD</a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Sign in</p>

			<?php echo $this->session->flashdata('message'); ?>
			<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

			<form method="post" action="" style="margin-bottom: 20px;">
				<div class="form-group has-feedback">
					<input type="email" name="email" class="form-control" placeholder="Email" />
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password" />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div>
				</div>
			</form>

			Don't have account yet? <a href="<?php echo base_url() ?>signup	">Sign up here.</a>

		</div>
	</div>
	<div class="text-center">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Class Portal Ads -->
	<ins class="adsbygoogle"
		style="display:block"
		data-ad-client="ca-pub-4437781027424280"
		data-ad-slot="1512986940"
		data-ad-format="auto"
		data-full-width-responsive="true"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	</div>
</body>
</html>