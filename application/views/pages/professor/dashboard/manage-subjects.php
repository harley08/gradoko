<div class="content-wrapper">
	<section class="content">
		<div class="box box-info shadow">
			<div class="box-header with-border">
				<h4 class="box-title">Manage Subjects</h4>
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-caret-square-o-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" data-toggle="modal" data-target="#addSubjectModal"><i class="fa fa-plus"></i> Add</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th class="text-center">Subject Code</th>
							<th class="text-center">Description</th>
							<th class="text-center">Sections</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody id="subjects"></tbody>
				</table>
			</div>
		</div>
	</section>
</div>