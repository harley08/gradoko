<div class="content-wrapper">
	<section class="content-header">
	</section>
	<section class="content">
		<div class="box shadow">
			<div class="box-header with-border">
				<h4 class="title">Set Active Semester & School Year</h4>
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-caret-square-o-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" data-toggle="modal" data-target="#SetNewSemModal"><i class="fa fa-plus"></i> Set New</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="text-center">Semester</th>
							<th class="text-center">Semestral Term</th>
							<th class="text-center">School Year</th>
							<th class="text-center">Status</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody id="semesterData"></tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<div id="successModal" class="modal modal-info" data-backdrop="static"data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><span class="glyphicon glyphicon-check"></span> Activated</h4>
			</div>
			<div class="modal-body">
				<p><span class="glyphicon glyphicon-check"></span> Semester and School Year Successfully Activated!</p>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<a href="<?php echo base_url() ?>" class="btn btn-default">Continue</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="SetNewSemModal" class="modal modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Set New</h4>				
			</div>
			<form id="setSemForm">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Semester:</label>
								<select name="semester" class="form-control">
									<option value="">-- choose --</option>
									<option value="First Semester">First Semester</option>
									<option value="Second Semester">Second Semester</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Semestral Term:</label>
								<select name="semestral_term" class="form-control">
									<option value="">-- choose --</option>
									<option value="Midterm">Midterm</option>
									<option value="Finals">Finals</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>School Year:</label>
								<input type="text" name="school_year" class="form-control" placeholder="e.g. 2017-2018" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-left">
						<button type="submit" class="btn btn-primary">Activate</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="confirmModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
			</div>
			<div class="modal-body">
				<h3 class="text-center">Are you sure you want to set this as active semester?</h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="yes btn btn-primary">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		showSemesters();
	});
	function showSemesters() {
		$.ajax({
			url: '<?php echo base_url('dashboard/get_semesters') ?>',
			dataType: 'json',
			success: function(data) {
				var semList = '';
				var i;
				for (i = 0; i < data.length; i++) {

					var status;
					var button

					if (data[i].activated == 1) {
						status = '<small class="label bg-green"><i class="fa fa-check"></i></small>';
						button = '<i class="fa fa-gear"></i>';
					} else {
						status = '<small class="label bg-red"><i class="fa fa-remove"></i></small>';
						button = '<a href="javascript:void(0);" onclick="setAsActive('+ data[i].id +')" data-toggle="tooltip" data-placement="top" title="Set as Active"><i class="fa fa-gear"></i></a>';
					}

					semList += '<tr>' + 
								'<td class="text-center">' + data[i].semester + '</td>' +
								'<td class="text-center">' + data[i].semestral_term + '</td>' +
								'<td class="text-center">' + data[i].school_year + '</td>' +
								'<td class="text-center">' + status + '</td>' +
								'<td class="text-center">' + button + '</td>' +	
								'</tr>';
				}

				if (data.length == 0) {
					semList = '<tr><td class="text-center" colspan="5">no active semester and school year yet</td></tr>';
				}

				$('#semesterData').html(semList);
			}
		});
	}

	$('#setSemForm').submit(function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url('dashboard/add_semester') ?>',
			dataType: 'json',
			data: $(this).serialize(),
			success: function() {
				$('#SetNewSemModal').modal('hide');
				$('#successModal').modal('show');
				showSemesters();
			}
		});
	});

	function setAsActive(id) {
		
		$('#confirmModal').modal('show');

		$('.yes').click(function(){
			$.ajax({
				url: '<?php echo base_url('dashboard/set_active_semester') ?>',
				type: 'POST',
				data: {semester_id: id},
				success: function() {
					$('#successModal').modal('show');
					$('#confirmModal').modal('hide');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert('Error: ' + thrownError);
					$('#submitting').hide();
				}
			});
		});

	}
</script>