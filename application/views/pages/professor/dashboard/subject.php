<style>
	.grading:hover {
		border: 1px solid #fff;
	}
</style>

<div class="content-wrapper">
	<section class="content-header">
		<h1>[<?php echo $subjectData['access_code'] ?>] <b><?php echo $subjectData['subject_code']; ?> <?php echo $subjectData['subject_description'] ?> - <?php echo $sectionData['section_name']  ?></b></h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
			<li>Subject</li>
			<li><?php echo $subjectData['subject_description']; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Students</span>
						<span class="info-box-number"><span id="totalStudents"></span><small></small></span>
					</div>
				</div>
			</div>
			<!-- <div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Incomplete</span>
						<span class="info-box-number">0<small></small></span>
					</div>
				</div>
			</div> -->
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-header with-border">
						<b>Students List</b>
						<div class="box-tools pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-box-tool dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-square-o-down"></i>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#" data-toggle="modal" data-target="#AddStudentModal"><i class="fa fa-plus"></i> Add New Student</a></li>
									<li><a href="#" data-toggle="modal" data-target="#checkAttendanceModal"><i class="fa fa-calendar-check-o"></i> Check Attendance</a></li>
									<li><a href="#" data-toggle="modal" data-target="#viewAttendanceModal"><i class="fa fa-calendar-check-o"></i> View Attendance</a></li>
									<li><a href="<?php echo base_url('dashboard/reports/') . $subjectData['subject_description'] . '/' . $subjectData['id'] . '/' . $subjectData['subject_code'] . '/' . $sectionData['id'] ?>" onclick="window.open('<?php echo base_url('dashboard/reports/') . $subjectData['subject_description'] . '/' . $subjectData['id'] . '/' . $subjectData['subject_code'] . '/' . $sectionData['id'] ?>', 'newwindows', 'width=1366, height=768'); return false;"><i class="fa fa-book"></i> Report Card</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body">
						<!-- <form method="post" action="<?php echo base_url('grade/show_update_grade') ?>">
							<input type="text" name="id">
							<button type="submit">Submit</button>
						</form> -->
						<table id="studentsTbl" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">Last Name</th>
									<th class="text-center">First Name</th>
									<th class="text-center">Middle Name</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody id="studentsLists"></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="showGrading">
			</div>
		</div>
	</section>
</div>