<div class="content-wrapper">
	<section class="content">
		<div class="box box-info shadow">
			<div class="box-header with-border">
				<h4 class="box-title">Grading System</h4>
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-box-tool dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-caret-square-o-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" data-toggle="collapse" data-target="#AddBaseGradeCollapse"><i class="fa fa-plus"></i> Add</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box-body">
				<form id="addBaseGradeForm">
				<div id="notiftxt"></div>
					<div class="row">
						<div class="col-md-3">
							<select name="subject_id" class="form-control" id="subjects">
							</select>
						</div>
						<div id="AddBaseGradeCollapse" class="collapse">
							<div class="col-md-3">
								<input type="text" name="base_grade" class="form-control" placeholder="Base Grade e.g. Recitation" required />
							</div>
							<div class="col-md-3">
								<input type="text" name="percentage" id="percentagetxt" class="form-control" placeholder="Percentage e.g. 0.2" required />
							</div>
							<div class="col-md-3">
								<div class="btn-group">
									<button type="submit" id="click" class="btn btn-primary">Add</button>
									<button type="button" id="closeBtn" data-toggle="collapse" data-target="#AddBaseGradeCollapse" class="btn btn-danger">Close</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<table class="table table-bordered" style="margin-top: 10px;">
					<thead>
						<tr>
							<th class="text-center">Base Grade</th>
							<th class="text-center">Percentage</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody id="gradingData"></tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<div id="UpdateModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Update Grade</h4>
			</div>
			<form id="updateGradeForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Base Grade:</label>
								<input type="text" name="base_grade" id="u_base_grade" class="form-control"  />
							</div>
							<div class="form-group">
								<label>Percentage:</label>
								<input type="text" name="percentage" id="u_percentage" class="form-control"  />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="u_id" />
					<input type="hidden" name="subject_id" id="u_subject_id" />
					<button type="submit" class="btn btn-primary">Update</button>
					<button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="deleteModal" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Delete</h4>
			</div>
			<div class="modal-body">
				<h3 class="text-center">Are you sure you want to remove this Base Grade?</h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary yes">Yes</button>
				<button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){

		showSubjects_SelectForm();

		$('#subjects').change(function(){
			
			var subject_id = $(this).val();

			$.ajax({
				url: '<?php echo base_url('grade/get') ?>',
				type: 'POST',
				dataType: 'json',
				data: {subject_id: subject_id},
				success: function(gradeData) {
					var subjectsInDropdown;
					var i;

					if (gradeData.length != 0) {
						for (i = 0; i < gradeData.length; i++) {
							var percentage = gradeData[i].percentage;
							percentage = percentage * 100;
							subjectsInDropdown += '<tr>' +
													'<td class="text-center">' + gradeData[i].base_grade + '</td>' +
													'<td class="text-center">' + percentage + '%</td>' +
													'<td class="text-center"><a href="javascript:void(0);" class="label bg-green edit_grade" data-id="' + gradeData[i].id + '" data-base_grade="'+ gradeData[i].base_grade +'" data-percentage="'+ gradeData[i].percentage +'" data-subject_id="'+ gradeData[i].subject_id +'">Edit</a> <a onclick="deleteGrade('+ gradeData[i].id +', '+ gradeData[i].subject_id +')" class="label bg-red">Delete</a></td>' +
												  '</tr>';
						}
					} else if (gradeData.length == 0) {
						subjectsInDropdown = '<tr><td colspan="3" class="text-center">no data</td></tr>';
					}

					$('#gradingData').html(subjectsInDropdown);

				},
		        error: function(xhr, ajaxOptions, thrownError)
		        {
		            alert("Error: " + thrownError);
		        }
			});

		});

	});

	function showGrading_afterEvent(subject_id) {
		$.ajax({
			url: '<?php echo base_url('grade/get') ?>',
			type: 'POST',
			dataType: 'json',
			data: {subject_id: subject_id},
			success: function(gradeData) {
				var subjectsInDropdown;
				var i;

				if (gradeData.length != 0) {
					for (i = 0; i < gradeData.length; i++) {
						var percentage = gradeData[i].percentage;
						percentage = percentage * 100;
						subjectsInDropdown += '<tr>' +
												'<td class="text-center">' + gradeData[i].base_grade + '</td>' +
												'<td class="text-center">' + percentage + '%</td>' +
												'<td class="text-center"><a href="javascript:void(0);" class="label bg-green edit_grade" data-id="' + gradeData[i].id + '" data-base_grade="'+ gradeData[i].base_grade +'" data-percentage="'+ gradeData[i].percentage +'" data-subject_id="'+ gradeData[i].subject_id +'">Edit</a> <a onclick="deleteGrade('+ gradeData[i].id +', '+ gradeData[i].subject_id +')" class="label bg-red">Delete</a></td>' +
											  '</tr>';
					}
				} else if (gradeData.length == 0) {
					subjectsInDropdown = '<tr><td colspan="3" class="text-center">no data</td></tr>';
				}

				$('#gradingData').html(subjectsInDropdown);

			},
	        error: function(xhr, ajaxOptions, thrownError)
	        {
	            alert("Error: " + thrownError);
	        }
		});
	}

	function showSubjects_SelectForm() {
		$.ajax({
				url: '<?php echo base_url('subject/get'); ?>',
				type: 'POST',
				dataType: 'json',
				data: {semester_id: '<?php echo $semester['id'] ?>'},
				success: function(subject) {
					var subjectsInDropdown;
					var i;

					subjectsInDropdown = '<option value="">-- choose --</option>';

					for (i = 0; i < subject.length; i++) {
						subjectsInDropdown += '<option value="' + subject[i].id + '">' + subject[i].subject_description + '</option>';
					}

					$('#subjects').html(subjectsInDropdown);
				}
			});
	}

	$('#addBaseGradeForm').submit(function(e){
		e.preventDefault();

		var subject_id = $('#subjects').val();

		$.ajax({
			url: '<?php echo base_url('grade/add') ?>',
			type: 'POST',
			dataType: 'html',
			data: $(this).serialize(),
			success: function()
			{
					$('#AddBaseGradeCollapse').collapse('hide');
					showGrading_afterEvent(subject_id);
			},
	        error: function(xhr, ajaxOptions, thrownError)
	        {
	            alert("Error: " + thrownError);
	        }
		});
	});

	$('#gradingData').on('click', '.edit_grade', function(){

		$('#UpdateModal').modal('show');

		var id = $(this).data('id');
		var base_grade = $(this).data('base_grade');
		var percentage = $(this).data('percentage');
		var subject_id = $(this).data('subject_id');

		$('#u_base_grade').val(base_grade);
		$('#u_percentage').val(percentage);
		$('#u_id').val(id);
		$('#u_subject_id').val(subject_id);

	});

	$('#updateGradeForm').submit(function(e){

		e.preventDefault();

		$.ajax({
			url: '<?php echo base_url('grade/update') ?>',
			type: 'POST',
			data: $(this).serialize(),
			success: function() {
				$('#UpdateModal').modal('hide');
				showGrading_afterEvent($('#u_subject_id').val());
			},
	        error: function(xhr, ajaxOptions, thrownError)
	        {
	            alert("Error: " + thrownError);
	        }
		});

	});

	function deleteGrade(id, subject_id) {

		$('#deleteModal').modal('show');

		$('.yes').click(function(){
			$.ajax({
				url: '<?php echo base_url('grade/delete') ?>',
				type: 'POST',
				data: {id: id},
				success: function() {
					$('#deleteModal').modal('hide');
					showGrading_afterEvent(subject_id);
				}
			});
		});

	}

	$(document).on('input', '#percentagetxt', function(){
		var percentage = $(this).val();
		var subject_id = $('#subjects').val();

		$.ajax({
			url: '<?php echo base_url('grade/check_subject_percentage') ?>',
			type: 'POST',
			data: {subject_id: subject_id, percentage: percentage},
			dataType: 'json',
			success: function(data)
			{
				var notif = '';

				if (data.exceeded == 1)
				{
					notif = '<div class="alert alert-danger">'+ data.details +'</div>';
					$('#click').prop('disabled', true);
				}
				else
				{
					$('#click').prop('disabled', false);
				}

				$('#notiftxt').html(notif);
			}
		});

	});

	$('#closeBtn').click(function(){
		$('#notiftxt').html('');
	});

</script>