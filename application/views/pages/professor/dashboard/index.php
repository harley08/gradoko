<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
    <h1>Dashboard</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Students</span>
              <span class="info-box-number"><span id="allStds"></span></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function(){

      $.ajax({
        url: '<?php echo base_url('student/count_all') ?>',
        dataType: 'json',
        success: function(data) {
          $('#allStds').html(data.length);
        },
			  error: function(xhr, ajaxOptions, thrownError) {
				  alert('Error: ' + thrownError);
        }
      });

    });
  </script>