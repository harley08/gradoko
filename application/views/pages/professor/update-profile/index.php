		<div class="content-wrapper">
			<section class="content">
				<div class="box">
					<div class="box-header with-border">
						<h5 class="box-title"><i class="glyphicon glyphicon-edit"></i> Update Profile</h5>
					</div>
					<form method="post" action="">						
						<div class="box-body">
							<?php echo $this->session->flashdata('message'); ?>
							<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label>First Name:</label>
										<input type="text" name="fname" class="form-control" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label>Middle Name:</label>
										<input type="text" name="mname" class="form-control" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label>Last Name:</label>
										<input type="text" name="lname" class="form-control" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label>Employment Status:</label>
										<select name="emp_status" class="form-control">
											<option value="">-- choose --</option>
											<option value="Part Time">Part Time</option>
											<option value="Full Time">Full Time</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label>Institution:</label>
										<textarea name="institution" class="form-control"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</form>
				</div>
			</section>
		</div>