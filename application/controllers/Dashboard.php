<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends CI_CONTROLLER {

		function __construct() { 	

 			parent::__construct();

 			date_default_timezone_set('Asia/Manila');
 			
 			$this->load->model('M_User', 'user');
 			$this->load->model('M_Subject', 'subject');

 			if (!$this->session->userdata('logged_in')) {
 				redirect('signin');
 			} else {
 				if ($this->session->userdata('activated') == 0 && $this->session->userdata('role') == 'Professor') {
 					redirect('update-profile');
 				} else if ($this->session->userdata('activated') == 0 && $this->session->userdata('role') == 'Student') {
 					redirect('student/update-profile');
 				} else if ($this->session->userdata('activated') == 1 && $this->session->userdata('role') == 'Student') {
 					redirect('student');
 				}
 			}

		}

		public function index() {
			$myid = $this->session->userdata('userid');
			$data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();

			$data['site'] = 'Professors Dashboard';
			$data['title'] = 'Dashboard';

			$this->load->view('templates/header', $data);
			$this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/dashboard/index');
			$this->load->view('templates/sidebar');
			$this->load->view('templates/footer');

		}

		public function grading() {
			$data['site'] = 'Grading System';

			$myid = $this->session->userdata('userid');
			$data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();

			$this->load->view('templates/header', $data);
			$this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/dashboard/grading-system');
			$this->load->view('templates/sidebar');
			$this->load->view('templates/footer');
		}

		public function semester() {
			$data['site'] = 'Semester & School Year Activation';

			$myid = $this->session->userdata('userid');
			$data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();

			$this->load->view('templates/header', $data);
			$this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/dashboard/semester');
			$this->load->view('templates/sidebar');
			$this->load->view('templates/footer');
		}

		public function get_semesters() {
			$prof_id = $this->session->userdata('userid');
			$data = $this->user->semesters($prof_id);
			echo json_encode($data);
		}

		public function set_active_semester() {
			
			$criteria = array(
				'prof_id' => $this->session->userdata('userid')
			);
			$update = array(
				'activated' => 0
			);

			$this->db->where($criteria);
			if ($this->db->update('semester', $update)) {
				$this->user->set_active_semester();
			}
		}

		public function add_semester() {
			$data = $this->user->addSemester();		
			echo json_encode($data);
		}

		public function subject($subject_description, $subject_id, $subject_code, $section_id) {
			
			$data['subjectData'] = $this->subject->get_subjectData($subject_id);
			$data['sectionData'] = $this->subject->get_section($section_id);

			$subjectData = $this->subject->get_subjectData($subject_id);

			$title = $subjectData['subject_description'];

			$data['site'] = $title;

			$myid = $this->session->userdata('userid');
			$data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();
			$data['section_id'] = $section_id;

			$this->load->view('templates/header', $data);
			$this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/dashboard/subject');
			$this->load->view('modals/subject');
			$this->load->view('js/subject');
			$this->load->view('templates/sidebar');
			$this->load->view('templates/footer');
		}

		public function manage_subjects() {

			$data['site'] = 'Manage Subjects';

			$myid = $this->session->userdata('userid');
			$data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();

			$this->load->view('templates/header', $data);
			$this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/dashboard/manage-subjects');
			$this->load->view('templates/sidebar');
			$this->load->view('templates/footer');
			$this->load->view('modals/manage-subjects');
			$this->load->view('js/manage-subjects');

		}

		public function reports($subject_description, $subject_id, $subject_code, $section_id)
		{
			$data['subjectData'] = $this->subject->get_subjectData($subject_id);
			$data['sectionData'] = $this->subject->get_section($section_id);

			$subjectData = $this->subject->get_subjectData($subject_id);
			$data['site'] = 'Report Card';
			$myid = $this->session->userdata('userid');
			$data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();
			$data['section_id'] = $section_id;
			$this->load->view('pages/reports/index', $data);
		}

	}
?>