<?php
	defined('BASEPATH') OR exit('No direct script access allowed!');

	class UpdateProfile extends CI_CONTROLLER {
		
		function __construct() {
			parent::__construct();
			date_default_timezone_set('Asia/Manila');
			if (!$this->session->userdata('logged_in')) redirect('login');
			$this->load->model('M_User', 'user');
		}		

		public function professor() {
			
			$id = $this->session->userdata['userid'];

			$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('emp_status', 'Employment Status', 'trim|required');
			$this->form_validation->set_rules('institution', 'Institution', 'trim|required');

			if ($this->form_validation->run()) {

				$lastname = $this->input->post('lname');
				$firstname = $this->input->post('fname');

				$data['firstname'] = $this->input->post('fname');
				$data['middlename'] = $this->input->post('mname');
				$data['lastname'] = $this->input->post('lname');
				$data['employment_status'] = $this->input->post('emp_status');
				$data['teaching_at'] = $this->input->post('institution');
				$data['userid'] = $id;
				$data['profile_link'] = strtolower($lastname) . '.' . strtolower($firstname) . '.' . $id;


				// update/insert data
				if ($this->user->update_profile($data, $id)) {
					
					$sess_activated = array('activated' => '1');
					$this->session->set_userdata($sess_activated);

					$this->session->set_flashdata('message', '<div class="alert alert-success">Profile updated.</div><div class="alert alert-success">Account Activated.</div>');
				}
			}
			$data['user'] = $this->user->getData($id);
			$data['semester'] = $this->user->getActiveSemester();


			$data['site'] = 'Update Profile';

			$this->load->view('templates/header', $data);
			$this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/update-profile/index');
			$this->load->view('templates/footer');
		}

		public function student() {
			$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('mobile', 'CP #', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');

			if ($this->form_validation->run()) {

				$id = $this->session->userdata['userid'];

				$data['userid'] = $id;
				$data['firstname'] = $this->input->post('fname');
				$data['middlename'] = $this->input->post('mname');
				$data['lastname'] = $this->input->post('lname');
				$data['mobile'] = $this->input->post('mobile');
				$data['address'] = $this->input->post('address');

				if ($this->user->student_update_profile($data, $id)) {
					
					$sess_activated = array('activated' => '1');
					$this->session->set_userdata($sess_activated);

					$this->session->set_flashdata('message', '<div class="alert alert-success">Profile updated.</div><div class="alert alert-success">Account Activated.</div>');
				}
			}

			$data['site'] = 'Update Profile';
			$this->load->view('pages/student/update-profile/index', $data);
		}

	}
?>