<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Signup extends CI_CONTROLLER {

		function __construct() {
			parent::__construct();
			date_default_timezone_set('Asia/Manila');
			$this->load->model('M_User', 'user');
		}

		public function index()
		{

			if ($this->session->userdata('logged_in')) redirect('dashboard');

			// form validation
			$this->form_validation->set_rules("role", "Role", "trim|required");
			$this->form_validation->set_rules("email", "Email", "trim|required");
			$this->form_validation->set_rules("password", "Password", "trim|required");

			if ($this->form_validation->run()) {
				$password = $this->input->post('password');				
				$key = $this->config->item('encryption_key');
				$salt3 = '!@#GH^%)-:HDF%';
				$salt1 = hash('sha512', $key . $password . $salt3);
				$salt2 = hash('sha512', $password . $key . $salt3);
				$hashed_password = hash('sha512', $salt1 . $password . $salt2 . $salt3);

				$data['role'] = $this->input->post('role');
				$data['email'] = $this->input->post('email');
				$data['password'] = $hashed_password;
				$data['activated'] = 0;
				$data['created_on'] = date('Y-m-d H:i:s');

				// insert data
				if ($this->user->insert_data($data)) {
					$this->session->set_flashdata('message', '<div class="alert alert-success">Account created successfully.</div>');
				}

			}

			$data['site'] = 'Sign up';

			$this->load->view('pages/signup/index', $data);
		}
	}
?>