<?php
	defined('BASEPATH') OR exit('No direct script access allowed!');
	
	class Subject extends CI_CONTROLLER {

		function __construct() {
			parent::__construct();
			date_default_timezone_set('Asia/Manila');
			$this->load->model('M_Subject', 'subject');
		}

		public function get() {
			$data = $this->subject->get();
			echo json_encode($data);
		}

		public function countGrading() {
			$data = $this->subject->countGrading();
			echo json_encode($data);
		}

		public function add() {
			$data = $this->subject->add();

			$no_of_sections = $this->input->post('no_of_sections');

			for ($i = 1; $i <= $no_of_sections; $i++) {

				$section_name = "Section $i";

				$data = array(
					'prof_id' => $this->session->userdata('userid'),
					'semester_id' => $this->input->post('semester_id'),
					'subject_code' => $this->input->post('subject_code'),
					'section_name' => $section_name
				);

				$section_data = $this->subject->add_section($data);
			}

			echo json_encode($data);

		}

		public function get_sections() {
			$data = $this->subject->get_sections();
			echo json_encode($data);
		}

		public function get_grading() {
			$data = $this->subject->get_grading();
			echo json_encode($data);
		}

		public function get_grading_student() {
			$data = $this->subject->get_grading_student();
			echo json_encode($data);
		}

		public function show_activities() {
			$data = $this->subject->show_activities();
			echo json_encode($data);
		}

		public function show_activities_student() {
			$data = $this->subject->show_activities_student();
			echo json_encode($data);
		}

		public function update()
		{
			$this->subject->update();
			$this->subject->update_subjectCode_inSections();
		}

		public function delete()
		{
			$this->subject->delete();
			$this->subject->delete_inSections();
			$this->subject->delete_inGrading();
			$this->subject->delete_inGrades();
			$this->subject->delete_inGrade();
			$this->subject->delete_inAttendance();
			$this->subject->delete_inStudents();
		}

		public function get_all_attendanceDate()
		{
			$data = $this->subject->get_all_attendanceDate();
			echo json_encode($data);
		}

	}

?>