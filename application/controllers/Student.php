<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Student extends CI_CONTROLLER {

		function __construct() {

			parent::__construct();
 			date_default_timezone_set('Asia/Manila');

 			$this->load->model('M_User', 'user');
 			$this->load->model('M_Student', 'student');
 			$this->load->model('M_Subject', 'subject');

 			if (!$this->session->userdata('logged_in')) {
 				redirect('signin');
 			} else {
 				if ($this->session->userdata('activated') == 0 && $this->session->userdata['role'] == 'Professor') {
 					redirect('update-profile');
 				}
 			}
		}

		public function index() {
			$data['site'] = 'Student Account';
			$data['user'] = $this->user->getDataStudents($this->session->userdata('userid'));

			$this->load->view('templates/header', $data);
			$this->load->view('templates/student/navigation');
			$this->load->view('templates/student/sidebar');
			$this->load->view('pages/student/index');
			$this->load->view('templates/sidebar');
			$this->load->view('templates/student/footer');
		}

		public function subject($access_code) {
			$user = $this->user->getDataStudents($this->session->userdata('userid'));
			$firstname = $user['firstname'];
			$middlename = $user['middlename'];
			$lastname = $user['lastname'];
			
			$get_subject = $this->db->query("SELECT * FROM subjects WHERE access_code = '$access_code'");
			$subject = $get_subject->row();
			$subject_id = $subject->id;

			$get = $this->db->query("SELECT * FROM students WHERE firstname = '$firstname' AND middlename = '$middlename' AND lastname = '$lastname' AND subject_id = $subject_id");
			$student_data = $get->row();

			$data['subjectData'] = $this->subject->get_subjectData($subject->id);
			$data['sectionData'] = $this->subject->get_section($student_data->section_id);

			$data['site'] = $subject->subject_description;
			$data['user'] = $this->user->getDataStudents($this->session->userdata('userid'));
			$data['section_id'] = $student_data->section_id;
			$data['student_id'] = $student_data->id;

			$this->load->view('templates/header', $data);
			$this->load->view('templates/student/navigation');
			$this->load->view('templates/student/sidebar');
			$this->load->view('pages/student/subject');
			$this->load->view('js/student/subject');
			$this->load->view('templates/student/footer');
		}

		public function show() {

			$prof_id = $this->session->userdata('userid');
			$semester = $this->user->getActiveSemester();
			$semester_id = $semester['id'];
			$subject_id = $this->input->post('subject_id');
			$section_id = $this->input->post('section_id');

			$data = $this->student->show($prof_id, $semester_id, $subject_id, $section_id);
			echo json_encode($data);
		}

		public function show_classmates()
		{
			$subject_id = $this->input->post('subject_id');
			$section_id = $this->input->post('section_id');

			$students = $this->db->query("SELECT * FROM students WHERE subject_id = $subject_id AND section_id = $section_id");
			echo json_encode($students->result());
		}

		public function count_all() {
			$prof_id = $this->session->userdata('userid');
			$semester = $this->user->getActiveSemester();
			$semester_id = $semester['id'];
			$data = $this->student->count_all($prof_id, $semester_id);
			echo json_encode($data);
		}

		public function show_name() {
			$data = $this->student->show_name();
			echo json_encode($data);
		}

		public function show_attendance() {
			$data = $this->student->show_attendance();
			echo json_encode($data);
		}

		public function update() {
			$data = $this->student->update();
		}

		public function show_update() {
			$student_id = $this->input->post('id');
			
			$data = $this->student->showUpdate($student_id);
			echo json_encode($data);
		}

		public function add() {
			$data = $this->student->add();
			echo json_encode($data);
		}

		public function attendance() {
			$n_student = count($this->input->post('student'));
			$std_id = $this->input->post('student');
			$status = $this->input->post('status');

			for ($i = 0; $i <= ($n_student - 1); $i++) {
				$student_id = $std_id[$i];
				$data = array(
					'prof_id' => $this->session->userdata('userid'),
					'semester_id' => $this->input->post('semester_id'),
					'subject_id' => $this->input->post('subject_id'),
					'section_id' => $this->input->post('section_id'),
					'student_id' => $std_id[$i],
					'status' => $status[$student_id],
					'date' => date('Y-m-d H:i:s')
				);
				$data = $this->student->attendance($data);
			}

		}

		public function get_subjects()
		{
			$criteria = array('userid' => $this->session->userdata('userid'));
			$this->db->where($criteria);
			$data = $this->db->get('student_subjects')->result();

			echo json_encode($data);
		}

		public function add_subject()
		{
			$userid = $this->session->userdata('userid');

			$subject_exists = 0;
			$student_exists = 0;

			$firstname = $this->input->post('firstname');
			$middlename = $this->input->post('middlename');
			$lastname = $this->input->post('lastname');

			$access_code = $this->input->post('access_code');
			$check_code = $this->db->query("SELECT * FROM subjects WHERE access_code = '$access_code'");
			$subjectData = $check_code->row();

			if ($check_code->num_rows() != 0)
			{
				$subject_id = $subjectData->id;
				$student = $this->db->query("SELECT * FROM students WHERE firstname = '$firstname' AND middlename = '$middlename' AND lastname = '$lastname' AND subject_id = $subject_id");
				if ($student->num_rows() != 0)
				{
					$subject_exists = 1;
					$student_exists = 1;
				}

				$this->db->query("INSERT INTO student_subjects (userid, access_code) VALUES ($userid, '$access_code')");
			}

			$notif = array(
				'subject_exists' => $subject_exists,
				'student_exists' => $student_exists
			);

			echo json_encode($notif);
		}

		public function get_subject_description()
		{
			$access_code = $this->input->post('access_code');

			$get_subject = $this->db->query("SELECT * FROM subjects WHERE access_code = '$access_code'");
			$subject = $get_subject->row();

			$data = array(
				'subject_code' => $subject->subject_code,
				'subject_description' => $subject->subject_description
			);

			echo json_encode($data);
		}

	}
?>