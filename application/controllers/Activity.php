<?php

    defined('BASEPATH') OR exit('No direct script access allowed!');

    class Activity extends CI_Controller {

        function __construct()
        {   
            parent::__construct();
            date_default_timezone_set('Asia/Manila');
        }

        public function show_update()
        {
            $criteria = array('id' => $this->input->post('id'));
            $this->db->where($criteria);
            $data = $this->db->get('grade');
            echo json_encode($data->result());
        }

        public function update()
        {
            $update = array(
                'details' => $this->input->post('details'),
                'total_points' => $this->input->post('total_points')
            );
            $criteria = array('id' => $this->input->post('id'));
            $this->db->where($criteria);
            $this->db->update('grade', $update);
        }

        public function delete()
        {
            $this->db->where('id', $this->input->post('id'));
            $this->db->delete('grade');
        }

    }

?>