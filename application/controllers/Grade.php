<?php
	defined('BASEPATH') OR exit('No direct script access allowed!');

	class Grade extends CI_CONTROLLER {

		function __construct() {
			parent::__construct();
			date_default_timezone_set('Asia/Manila');
			$this->load->model('M_Grade', 'grade');
		}

		public function add() {
			$data = $this->grade->add();
			echo json_encode($data);
		}

		public function addTo_baseGrade() {
			$data = $this->grade->addTo_baseGrade();
			echo json_encode($data);
		}

		public function get() {
			$data = $this->grade->get();
			echo json_encode($data);
		}

		public function get_activity()
		{
			$criteria = array(
				'id' => $this->input->post('id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('grade');
			echo json_encode($data->result());
		}

		public function show_update() {
			$data = $this->grade->show_update();
			echo json_encode($data);
		}

		public function show_update_grade() {
			$data = $this->grade->show_update_grade();
			echo json_encode($data);
		}

		public function update() {
			$this->grade->update();
		}

		public function update_score()
		{
			$this->grade->update_score();
		}

		public function delete() {
			$this->grade->delete();
		}

		public function encode()
		{
			$count_students = count($this->input->post('student'));
			$student_id = $this->input->post('student');
			$score = $this->input->post('score');

			for ($i = 0; $i <= ($count_students - 1); $i++)
			{
				$std_id = $student_id[$i];
				$data = array(
					'prof_id' => $this->session->userdata('userid'),
					'subject_id' => $this->input->post('subject_id'),
					'section_id' => $this->input->post('section_id'),
					'grading_id' => $this->input->post('grading_id'),
					'grade_id' => $this->input->post('grade_id'),
					'student_id' => $std_id,
					'points' => $score[$std_id]
				);

				$this->grade->encode($data);
			}

		}

		public function show_per_activity()
		{
			$data = $this->grade->show_per_activity();
			echo json_encode($data);
		}

		public function show_per_activity_student()
		{
			$data = $this->grade->show_per_activity_student();
			echo json_encode($data);
		}

		public function show_per_activity_student_me()
		{
			$data = $this->grade->show_per_activity_student_me();
			echo json_encode($data);
		}

		public function check_subject_percentage()
		{
			$subject_id = $this->input->post('subject_id');
			$percentage = $this->input->post('percentage');
			$prof_id = $this->session->userdata('userid');

			$percent = 0;
			$total_percentage = 0;

			$q = $this->db->query("SELECT * FROM grading WHERE subject_id = $subject_id AND prof_id = $prof_id");
			foreach ($q->result() as $data)
			{
				$percent += ($data->percentage * 100);
			}

			$total_percentage = $percent + $percentage;

			if ($total_percentage > 100)
			{
				$notif = array(
					'details' => 'Total percentage must not be exceed to 101% and above.',
					'exceeded' => '1'
				);
			}
			else
			{
				$notif = array(
					'details' => '',
					'exceeded' => '0'
				);
			}

			echo json_encode($notif);

		}

		public function addAttendance_ToBaseGrade()
		{
			$grading_id = $this->input->post('grading_id');
			$prof_id = $this->session->userdata('userid');
			$subject_id = $this->input->post('subject_id');
			$section_id = $this->input->post('section_id');
			$details = $this->input->post('details');
			if ($this->grade->addTo_baseGrade())
			{
				$get_grading = $this->db->query("SELECT * FROM grade WHERE details = '$details' AND prof_id = $prof_id AND subject_id = $subject_id AND section_id = $section_id");
				foreach ($get_grading->result() as $gradeData)
				{
					$grade_id = $gradeData->id;
					$students = $this->db->query("SELECT * FROM students WHERE prof_id = $prof_id AND subject_id = $subject_id AND section_id = $section_id");
					foreach ($students->result() as $studentData)
					{
						$student_id = $studentData->id;
						$get_attendance = $this->db->query("SELECT * FROM attendance WHERE prof_id = $prof_id AND subject_id = $subject_id AND student_id = $student_id AND section_id = $section_id AND status = 'present'");

						$total_presents = 0;
						foreach ($get_attendance->result() as $attendanceData)
						{
							++$total_presents;
						}

						$this->db->query("INSERT INTO grades (prof_id, subject_id, section_id, grading_id, grade_id, student_id, points) VALUES ($prof_id, $subject_id, $section_id, $grade_id, $grading_id, $student_id, $total_presents)");
						
					}
				}
			}
		}

		public function checkAttendanceIfAddedToGrade()
		{
			$prof_id = $this->session->userdata('userid');
			$subject_id = $this->input->post('subject_id');
			$section_id = $this->input->post('section_id');
			$details = 'Student Attendance';

			$check = $this->db->query("SELECT * FROM grade WHERE prof_id = $prof_id AND subject_id = $subject_id AND section_id = $section_id AND details = '$details'");

			$data = array(
				'exist' => $check->num_rows()
			);

			echo json_encode($data);
		}

	}
?>