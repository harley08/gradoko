<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed!');

    class Profile extends CI_CONTROLLER {

        function __construct() {

            parent::__construct();

            date_default_timezone_set('Asia/Manila');

            $this->load->model('M_Profile', 'profile');
            $this->load->model('M_User', 'user');

            if (!$this->session->userdata('logged_in')) {
                redirect('signin');
            } else {
                if ($this->session->userdata('activated') == 0 && $this->session->userdata('role') == 'Professor') {
                    redirect('update-profile');
                } else if ($this->session->userdata('activated') == 0 && $this->session->userdata('role') == 'Student') {
                    redirect('student/update-profile');
                }
            }

        }

        public function index($profile_link) {

            $myid = $this->session->userdata('userid');

            $getData = $this->profile->get($profile_link);

            $title = 'Prof. ' . $getData['firstname'] . ' ' . $getData['lastname'] . "'s Profile";

            $data['site'] = $title;
            $data['user'] = $this->user->getData($myid);
			$data['semester'] = $this->user->getActiveSemester();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/professor/navigation');
			$this->load->view('templates/professor/sidebar');
			$this->load->view('pages/professor/profile/index');
			$this->load->view('templates/footer');

        }

    }

?>