<?php	
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Signin extends CI_CONTROLLER {

		function __construct() { 			
 			parent::__construct();
 			date_default_timezone_set('Asia/Manila');
 			$this->load->model('M_User', 'user');
		}

		public function index() {

			if ($this->session->userdata('logged_in')) redirect('dashboard');

			$this->form_validation->set_rules("email", "Email", "trim|required");
			$this->form_validation->set_rules("password", "Password", "trim|required");

			if ($this->form_validation->run()) {

				// get form inputs
				$password = $this->input->post('password');
				$email = $this->input->post('email');

				// check user credentials
				$result = $this->user->get_user($email, $password);
				if (count($result)) {
					// set session
					$sess_data = array (
						'userid' => $result['id'],
						'email' => $result['email'],
						'role' => $result['role'],
						'activated' => $result['activated'],
						'logged_in' => TRUE
					);

					$this->session->set_userdata($sess_data);
					redirect('dashboard');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger text-center">Wrong Username or Password!</div>');
				}

			}

			$data['site'] = 'Sign in';

			$this->load->view('pages/signin/index', $data);
		}

		public function signout() {
			$sess = array('logged_in' => FALSE, 'userid' => '', 'email' => '', 'role' => '', 'activated' => '');
			$this->session->unset_userdata($sess);
			$this->session->sess_destroy();
			redirect('signin');
		}

	}
?>