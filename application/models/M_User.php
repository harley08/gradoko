<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed!');

	class M_User extends CI_Model {

		function __construct() {
			parent::__construct();
		}

		public function insert_data($data) {
			return $this->db->insert('accounts', $data);
		}

		public function get_user($email, $password) {
			$key = $this->config->item('encryption_key');
	        $salt3 = '!@#GH^%)-:HDF%';
	        $salt1 = hash('sha512', $key . $password  . $salt3);
	        $salt2 = hash('sha512', $password . $key . $salt3);
	        $hashed_password = hash('sha512', $salt1 . $password . $salt2 . $salt3);
	        $password = $hashed_password;

	        $data = array();
	        $details = array('email' => $email, 'password' => $password);
	        $this->db->where($details);
	        $this->db->limit(1);
	        $q = $this->db->get('accounts');

	        if ($q->num_rows() > 0) {
	        	$data = $q->row_array();
	        }

	        $q->free_result();
	        return $data;
		}

		public function update_profile($data, $id) {
			$criteria = array('id' => $id);
			$this->db->where($criteria);
			$q = $this->db->get('professors');

			if ($q->num_rows() == null) {
				// activate professor account
				$activated = array('activated' => '1');
				$this->db->where('id', $id);
				$this->db->set($activated);

				if ($this->db->update('accounts')) {
					// insert professors info
					return $this->db->insert('professors', $data);
				}
			}
		}

		public function student_update_profile($data, $id) {
			$criteria = array('userid' => $id);
			$this->db->where($criteria);
			$q = $this->db->get('reg_students');

			if ($q->num_rows() == null) {
				// activate professor account
				$activated = array('activated' => '1');
				$this->db->where('id', $id);
				$this->db->set($activated);

				if ($this->db->update('accounts')) {
					// insert professors info
					return $this->db->insert('reg_students', $data);
				}
			}
		}

		public function getData($myid) {
			$data = array();
			$this->db->where('userid', $myid);
			$this->db->limit(1);
			$q = $this->db->get('professors');
			if ($q->num_rows() > 0) {
				$data = $q->row_array();
			}
			$q->free_result();
			return $data;
		}

		public function getDataStudents($myid) {
			$data = array();
			$this->db->where('userid', $myid);
			$this->db->limit(1);
			$q = $this->db->get('reg_students');
			if ($q->num_rows() > 0) {
				$data = $q->row_array();
			}
			$q->free_result();
			return $data;
		}

		public function getActiveSemester() {
			$prof_id = $this->session->userdata('userid');

			$data = array();
			$criteria = array(
				'prof_id' => $prof_id,
				'activated' => '1'
			);
			$this->db->where($criteria);
			$this->db->limit(1);
			$this->db->order_by('id', 'DESC');
			$q = $this->db->get('semester');
			if ($q->num_rows() > 0) {
				$data = $q->row_array();
			}
			$q->free_result();
			return $data;
		}

		public function semesters($prof_id) {
			$criteria = array('prof_id' => $prof_id);

			$this->db->where($criteria);
			$this->db->order_by('id', 'DESC');
			$data = $this->db->get('semester');
			return $data->result();
		}

		public function set_active_semester() {
			
			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'id' => $this->input->post('semester_id')
			);
			$update = array(
				'activated' => 1
			);

			$this->db->where($criteria);
			return $this->db->update('semester', $update);

		}

		public function addSemester() {
			$semester = $this->db->get('semester');
			if ($semester->num_rows() == null) {
				$data = array(
				'prof_id' => $this->session->userdata('userid'),
				'semester' => $this->input->post('semester'),
				'semestral_term' => $this->input->post('semestral_term'),
				'school_year' => $this->input->post('school_year'),
				'activated' => '1'
				);
				$result = $this->db->insert('semester', $data);
				return $result;
			} else {
				$upd['activated'] = 0;
				$prof_id = $this->session->userdata('userid');
				$this->db->where('prof_id', $prof_id);
				$update = $this->db->update('semester', $upd);
				if ($update) {
					$data = array(
					'prof_id' => $this->session->userdata('userid'),
					'semester' => $this->input->post('semester'),
					'semestral_term' => $this->input->post('semestral_term'),
					'school_year' => $this->input->post('school_year'),
					'activated' => '1'
					);
					$result = $this->db->insert('semester', $data);
					return $result;
				}
			}
		}
		
	}
?>