<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed!');

	class M_Subject extends CI_MODEL {

		function __construct() {
			
			parent::__construct();
		}

		public function add() {

			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$code = array();
			$alphaLength = strlen($alphabet) - 1;
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$code[] = $alphabet[$n];
			}

			$access_code = implode($code);

			$data = array(
				'prof_id' => $this->session->userdata('userid'),
				'semester_id' => $this->input->post('semester_id'),
				'subject_code' => $this->input->post('subject_code'),
				'subject_description' => $this->input->post('subject_description'),
				'no_of_sections' => $this->input->post('no_of_sections'),
				'access_code' => $access_code,
				'date_created' => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('subjects', $data);
			return $result;
		}

		public function countGrading() {

			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'subject_id' => $this->input->post('subject_id'),
				'section_id' => $this->input->post('section_id'),
				'grade_id' => $this->input->post('grade_id')
			);

			$this->db->where($criteria);
			$data = $this->db->get('grade');
			return $data->result();

		}

		public function get() {
			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'semester_id' => $this->input->post('semester_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('subjects');
			return $data->result();
		}

		public function get_grading() {
			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'subject_id' => $this->input->post('subject_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('grading');
			return $data->result();
		}

		public function get_grading_student() {
			$criteria = array(
				'subject_id' => $this->input->post('subject_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('grading');
			return $data->result();
		}

		public function get_subjectData($subject_id) {
			$criteria = array('id' => $subject_id);
			$this->db->where($criteria);
			$this->db->limit(1);
			$q = $this->db->get('subjects');
			if ($q->num_rows() > 0) {
				$data = $q->row_array();
			}
			$q->free_result();
			return $data;

		}

		public function get_section($section_id) {
			$criteria = array('id' => $section_id);
			$this->db->where($criteria);
			$this->db->limit(1);
			$q = $this->db->get('sections');
			if ($q->num_rows() > 0) {
				$data = $q->row_array();
			}
			$q->free_result();
			return $data;
		}

		public function get_sectionData($subject_id) {
			$criteria = array('id' => $subject_id);
			$this->db->where($criteria);
			$this->db->limit(1);
			$q = $this->db->get('subjects');
			if ($q->num_rows() > 0) {
				$data = $q->row_array();
			}
			$q->free_result();
			return $data;

		}

		public function get_sections() {
			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'semester_id' => $this->input->post('semester_id'),
				'subject_code' => $this->input->post('subject_code')
			);
			$this->db->where($criteria);
			$data = $this->db->get('sections');
			return $data->result();
		}

		public function add_section($data) {
			$result = $this->db->insert('sections', $data);
			return $result;
		}

		public function show_activities()
		{
			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'subject_id' => $this->input->post('subject_id'),
				'section_id' => $this->input->post('section_id'),
				'grade_id' => $this->input->post('grade_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('grade');
			return $data->result();
		}

		public function show_activities_student()
		{
			$criteria = array(
				'subject_id' => $this->input->post('subject_id'),
				'section_id' => $this->input->post('section_id'),
				'grade_id' => $this->input->post('grade_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('grade');
			return $data->result();
		}

		public function update()
		{
			$criteria = array(
				'id' => $this->input->post('id')
			);
			$update = array(
				'subject_code' => $this->input->post('code'),
				'subject_description' => $this->input->post('description')
			);
			$this->db->where($criteria);
			return $this->db->update('subjects', $update);
		}

		public function update_subjectCode_inSections()
		{
			$criteria = array('subject_code' => $this->input->post('curr_code'));
			$update = array('subject_code' => $this->input->post('code'));
			$this->db->where($criteria);
			return $this->db->update('sections', $update);
		}

		public function delete()
		{
			$this->db->where('id', $this->input->post('id'));
			return $this->db->delete('subjects');
		}

		public function delete_inSections()
		{
			$this->db->where('subject_code', $this->input->post('subject_code'));
			return $this->db->delete('sections');
		}

		public function delete_inGrading()
		{
			$this->db->where('subject_id', $this->input->post('id'));
			return $this->db->delete('grading');
		}

		public function delete_inGrades()
		{
			$this->db->where('subject_id', $this->input->post('id'));
			return $this->db->delete('grades');
		}

		public function delete_inGrade()
		{
			$this->db->where('subject_id', $this->input->post('id'));
			return $this->db->delete('grade');
		}

		public function delete_inAttendance()
		{
			$this->db->where('subject_id', $this->input->post('id'));
			return $this->db->delete('attendance');
		}

		public function delete_inStudents()
		{
			$this->db->where('subject_id', $this->input->post('id'));
			return $this->db->delete('students');
		}

		public function get_all_attendanceDate()
		{
			$criteria = array(
				'attendance.prof_id' => $this->session->userdata('userid'),
				'attendance.subject_id' => $this->input->post('subject_id'),
				'attendance.semester_id' => $this->input->post('semester_id'),
				'attendance.section_id' => $this->input->post('section_id')
			);
			$this->db->where($criteria);
			$this->db->group_by('date');
			$data = $this->db->get('attendance');
			return $data->result();
		}

	}
?>