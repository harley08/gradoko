<?php

    if (!defined('BASEPATH')) exit('No direct script access allowed!');

    class M_Profile extends CI_Model {

        function __construct() {

            parent::__construct();

        }

        public function get($profile_link) {

            $criteria = array(
                'profile_link' => $profile_link
            );

            $this->db->where($criteria);
            $this->db->limit(1);

            $q = $this->db->get('professors');

            if ($q->num_rows() > 0) {
                $data = $q->row_array();
            }

            $q->free_result();
            return $data;


        }

    }

?>