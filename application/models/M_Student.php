<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed!');

	class M_Student extends CI_MODEL {

		function __construct() {
			
			parent::__construct();
		}

		public function count_all($prof_id, $semester_id) {
			
			$criteria = array(
				'prof_id' => $prof_id,
				'semester_id' => $semester_id

			);
			$this->db->where($criteria);
			$data = $this->db->get('students');
			return $data->result();
			
		}

		public function show($prof_id, $semester_id, $subject_id, $section_id) {

			$criteria = array(
				'prof_id' => $prof_id,
				'semester_id' => $semester_id,
				'subject_id' => $subject_id,
				'section_id' => $section_id

			);
			$this->db->where($criteria);
			$data = $this->db->get('students');
			return $data->result();
			
		}

		public function show_name() {

			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'id' => $this->input->post('student_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('students');
			return $data->result();
		}

		public function show_attendance() {
			
			$criteria = array(
				'attendance.prof_id' => $this->session->userdata('userid'),
				'attendance.subject_id' => $this->input->post('subject_id'),
				'attendance.semester_id' => $this->input->post('semester_id'),
				'attendance.section_id' => $this->input->post('section_id'),
				'attendance.date' => $this->input->post('date')
			);
			$this->db->select('*');
			$this->db->from('attendance');
			$this->db->join('students', 'students.id = attendance.student_id');
			$this->db->where($criteria);
			$data = $this->db->get();
			return $data->result();
		}

		public function showUpdate($student_id) {

			$criteria = array('id' => $student_id);

			$this->db->where($criteria);
			$data = $this->db->get('students');
			return $data->result();

		}

		public function add() {
			$data = array(
				'prof_id' => $this->session->userdata('userid'),
				'semester_id' => $this->input->post('semester_id'),
				'subject_id' => $this->input->post('subject_id'),
				'section_id' => $this->input->post('section_id'),
				'firstname' => $this->input->post('fname'),
				'middlename' => $this->input->post('mname'),
				'lastname' => $this->input->post('lname'),
			);
			$result = $this->db->insert('students', $data);
			return $result;
		}

		public function attendance($data) {
			$result = $this->db->insert('attendance', $data);
			return $result;
		}

		public function update() {
			$update = array(
				'firstname' => $this->input->post('fname'),
				'middlename' => $this->input->post('mname'),
				'lastname' => $this->input->post('lname')
			);
			$criteria = array('id' => $this->input->post('id'));

			$this->db->where($criteria);
			return $this->db->update('students', $update);
		}

	}
?>