<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed!');

	class M_Grade extends CI_MODEL {

		function __construct() {
			parent::__construct();
		}

		public function add() {
			$percentage = $this->input->post('percentage');
			$percentage = $percentage / 100;

			$data = array(
				'prof_id' => $this->session->userdata('userid'),
				'subject_id' => $this->input->post('subject_id'),
				'base_grade' => $this->input->post('base_grade'),
				'percentage' => $percentage
			);

			$result = $this->db->insert('grading', $data);
			return $result;
		}

		public function addTo_baseGrade() {
			$data = array(
				'prof_id' => $this->session->userdata('userid'),
				'subject_id' => $this->input->post('subject_id'),
				'section_id' => $this->input->post('section_id'),
				'grade_id' => $this->input->post('grade_id'),
				'details' => $this->input->post('details'),
				'total_points' => $this->input->post('total_points')
			);

			$result = $this->db->insert('grade', $data);
			return $result;
		}

		public function get() {
			$criteria = array(
				'prof_id' => $this->session->userdata('userid'),
				'subject_id' => $this->input->post('subject_id')
			);
			$this->db->where($criteria);
			$data = $this->db->get('grading');
			return $data->result();
		}

		public function show_update() {
			
			$criteria = array(
				'id' => $this->input->post('id')
			);

			$this->db->where($criteria);
			$data = $this->db->get('grading');
			return $data->result();

		}

		public function update() {

			$criteria = array(
				'id' => $this->input->post('id')
			);

			$update = array(
				'base_grade' => $this->input->post('base_grade'),
				'percentage' => $this->input->post('percentage')
			);

			$this->db->where($criteria);
			return $this->db->update('grading', $update);

		}

		public function update_score()
		{
			$criteria = array('id' => $this->input->post('id'));
			$update = array('points' => $this->input->post('points'));
			$this->db->where($criteria);
			return $this->db->update('grades', $update);
		}

		public function delete() {

			$this->db->where('id', $this->input->post('id'));
			return $this->db->delete('grading');

		}

		public function encode($data)
		{
			$encode = $this->db->insert('grades', $data);
			return $encode;
		}

		public function show_per_activity()
		{
			$criteria = array(
				'grades.prof_id' => $this->session->userdata('userid'),
				'grades.subject_id' => $this->input->post('subject_id'),
				'grades.section_id' => $this->input->post('section_id'),
				'grades.grading_id' => $this->input->post('grading_id'),
				'grades.grade_id' => $this->input->post('grade_id')
			);

			$this->db->select('*');
			$this->db->from('students');
			$this->db->join('grades', 'students.id = grades.student_id');
			$this->db->where($criteria);
			$data = $this->db->get();
			return $data->result();
		}

		public function show_per_activity_student()
		{
			$criteria = array(
				'grades.subject_id' => $this->input->post('subject_id'),
				'grades.section_id' => $this->input->post('section_id'),
				'grades.grading_id' => $this->input->post('grading_id'),
				'grades.grade_id' => $this->input->post('grade_id')
			);

			$this->db->select('*');
			$this->db->from('students');
			$this->db->join('grades', 'students.id = grades.student_id');
			$this->db->where($criteria);
			$data = $this->db->get();
			return $data->result();
		}

		public function show_per_activity_student_me()
		{
			$criteria = array(
				'grades.subject_id' => $this->input->post('subject_id'),
				'grades.section_id' => $this->input->post('section_id'),
				'grades.grading_id' => $this->input->post('grading_id'),
				'grades.grade_id' => $this->input->post('grade_id'),
				'grades.student_id' => $this->input->post('student_id')
			);

			$this->db->select('*');
			$this->db->from('students');
			$this->db->join('grades', 'students.id = grades.student_id');
			$this->db->where($criteria);
			$data = $this->db->get();
			return $data->result();
		}

		public function show_update_grade()
		{
			$criteria = array(
				'grades.prof_id' => $this->session->userdata('userid'),
				'grades.id' => $this->input->post('id')
			);
			$this->db->select('*');
			$this->db->from('grades');
			$this->db->join('students', 'students.id = grades.student_id');
			$this->db->where($criteria);
			$data = $this->db->get();
			return $data->result();
		}

	}
?>