//Password checker
$(document).ready(function() {
    $('#submitBtn').attr('disabled', 'disabled');
    var pass       = $('#password'); //id of first password field
    var cpass       = $('#cpassword'); //id of second password field
    var passInfo   = $('#pass-info'); //id of indicator element
    
    passwordStrengthCheck(pass,cpass,passInfo); //call password check function
    
});

function passwordStrengthCheck(pass, cpass, passInfo)
{
    //Must contain 5 characters or more
    var WeakPass = /(?=.{5,}).*/; 
    //Must contain lower case letters and at least one digit.
    var MediumPass = /^(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/; 
    //Must contain at least one upper case letter, one lower case letter and one digit.
    var StrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/; 
    //Must contain at least one upper case letter, one lower case letter and one digit.
    var VryStrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=\S*?[^\w\*])\S{5,}$/; 
    
    $(pass).on('keyup', function(e) {
        if(VryStrongPass.test(pass.val()))
        {
            passInfo.removeClass().addClass('vrystrongpass').html("Very Strong! (Awesome, please don't forget your pass now!)");
            $('#submitBtn').removeAttr('disabled');
        }   
        else if(StrongPass.test(pass.val()))
        {
            passInfo.removeClass().addClass('strongpass').html("Strong! (Enter special chars to make even stronger");
            $('#submitBtn').removeAttr('disabled');
        }   
        else if(MediumPass.test(pass.val()))
        {
            passInfo.removeClass().addClass('goodpass').html("Good! (Enter uppercase letter to make strong)");
            $('#submitBtn').removeAttr('disabled');
        }
        else if(WeakPass.test(pass.val()))
        {
            passInfo.removeClass().addClass('stillweakpass').html("Still Weak! (Enter digits to make good password)");
            $('#submitBtn').attr('disabled', 'disabled');
        }
        else
        {
            passInfo.removeClass().addClass('weakpass').html("Very Weak! (Must be 5 or more chars)");
            $('#submitBtn').attr('disabled', 'disabled');
        }
    });
    
    $(cpass).on('keyup', function(e) {
        
        if(pass.val() !== cpass.val())
        {
            passInfo.removeClass().addClass('weakpass').html("Passwords do not match!");   
            $('#submitBtn').attr('disabled', 'disabled');
        }else{
            passInfo.removeClass().addClass('goodpass').html("Passwords match!");
            $('#submitBtn').removeAttr('disabled');
        }
            
    });
}